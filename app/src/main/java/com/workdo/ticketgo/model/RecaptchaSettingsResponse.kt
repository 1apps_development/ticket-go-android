package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class RecaptchaSettingsResponse(

	@field:SerializedName("data")
	val data: RecaptchaData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Recaptcha(

	@field:SerializedName("RECAPTCHA_MODULE")
	val rECAPTCHAMODULE: String? = null,

	@field:SerializedName("NOCAPTCHA_SITEKEY")
	val nOCAPTCHASITEKEY: String? = null,

	@field:SerializedName("NOCAPTCHA_SECRET")
	val nOCAPTCHASECRET: String? = null
)

data class RecaptchaData(

	@field:SerializedName("recaptcha")
	val recaptcha: Recaptcha? = null
)
