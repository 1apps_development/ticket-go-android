package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class LanguageResponse(

	@field:SerializedName("data")
	val data: LanguageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class LanguageData(

	@field:SerializedName("language")
	val language: ArrayList<String>? = null
)
