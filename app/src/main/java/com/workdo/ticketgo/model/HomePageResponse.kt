package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class HomePageResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class LastTicketItem(

	@field:SerializedName("note")
	val note: Any? = null,

	@field:SerializedName("attachments")
	val attachments: ArrayList<String>? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("subject")
	val subject: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("ticket_id")
	val ticketId: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class YAxisItem(

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<Int>? = null,

	@field:SerializedName("name")
	val name: String? = null
)

data class GraphData(

	@field:SerializedName("y_axis")
	val yAxis: ArrayList<YAxisItem>? = null,

	@field:SerializedName("x_axis")
	val xAxis: ArrayList<String>? = null
)

data class UserData(

	@field:SerializedName("total_ticket")
	val totalTicket: Int? = null,

	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class CategoryAnalyticsItem(

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class TicketAnalytics(

	@field:SerializedName("close_ticket")
	val closeTicket: Int? = null,

	@field:SerializedName("open_ticket")
	val openTicket: Int? = null,

	@field:SerializedName("new_ticket")
	val newTicket: Int? = null
)

data class Statistics(

	@field:SerializedName("close_ticket")
	val closeTicket: Int? = null,

	@field:SerializedName("open_ticket")
	val openTicket: Int? = null,

	@field:SerializedName("category")
	val category: Int? = null,

	@field:SerializedName("agents")
	val agents: Int? = null
)

data class Data(

	@field:SerializedName("last_ticket")
	val lastTicket: ArrayList<TicketDataItem>? = null,

	@field:SerializedName("graph_data")
	val graphData: GraphData? = null,

	@field:SerializedName("user_data")
	val userData: UserData? = null,

	@field:SerializedName("category_analytics")
	val categoryAnalytics: ArrayList<CategoryAnalyticsItem>? = null,

	@field:SerializedName("ticket_analytics")
	val ticketAnalytics: TicketAnalytics? = null,

	@field:SerializedName("statistics")
	val statistics: Statistics? = null
)
