package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class DeleteTicketResponse(

	@field:SerializedName("data")
	val data: DeleteData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class DeleteData(

	@field:SerializedName("ticket")
	val ticket: ArrayList<TicketDataItem>? = null
)
