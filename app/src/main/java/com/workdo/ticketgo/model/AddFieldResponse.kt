package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class AddFieldResponse(

	@field:SerializedName("data")
	val data: CustomFiledData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CustomFiledData(

	@field:SerializedName("custom_field")
	val customField: CustomField? = null
)

data class CustomField(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("placeholder")
	val placeholder: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("created_by")
	val createdBy: String? = null
)
