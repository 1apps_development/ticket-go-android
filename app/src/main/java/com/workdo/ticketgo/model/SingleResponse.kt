package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class SingleResponse(
	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("logout")
	val logOut:Int?=null,

	@field:SerializedName("data")
	val data: ErrorData?=null
)

data class ErrorData(
	@field:SerializedName("message")
	val message: String? = null,
)
