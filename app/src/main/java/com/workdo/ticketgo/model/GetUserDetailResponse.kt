package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class GetUserDetailResponse(

	@field:SerializedName("data")
	val data: UsersDetailData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class UsersDetail(

	@field:SerializedName("parent")
	val parent: Int? = null,

	@field:SerializedName("is_active")
	val isActive: Int? = null,

	@field:SerializedName("role")
	val role: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("email_verified_at")
	val emailVerifiedAt: String? = null,

	@field:SerializedName("device_type")
	val deviceType: String? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("avatarlink")
	val avatarlink: String? = null,

	@field:SerializedName("requested_plan")
	val requestedPlan: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("plan_expire_date")
	val planExpireDate: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("lang")
	val lang: String? = null,

	@field:SerializedName("plan")
	val plan: Int? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null
)

data class UsersDetailData(

	@field:SerializedName("users")
	val users: UsersDetail? = null
)
