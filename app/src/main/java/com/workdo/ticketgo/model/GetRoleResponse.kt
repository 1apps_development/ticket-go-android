package com.workdo.ticketgo.model

import android.widget.ArrayAdapter
import com.google.gson.annotations.SerializedName

data class GetRoleResponse(

	@field:SerializedName("data")
	val data: RoleData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class RoleItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("guard_name")
	val guardName: String? = null
)

data class RoleData(

	@field:SerializedName("role")
	val role: ArrayList<RoleItem>? = null
)
