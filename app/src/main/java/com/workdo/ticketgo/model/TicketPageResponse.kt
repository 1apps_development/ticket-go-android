package com.workdo.ticketgo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class TicketPageResponse(

	@field:SerializedName("data")
	val data: TicketData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class StatusItem(

	@field:SerializedName("tickets")
	val tickets: ArrayList<TicketDataItem>? = null,

	@field:SerializedName("status")
	val status: String? = null,

	var expandable: Boolean = false

)

data class TicketData(

	@field:SerializedName("analytics")
	val analytics: Analytics? = null,

	@field:SerializedName("ticket")
	val ticket: Ticket? = null,

	@field:SerializedName("status")
	val status: ArrayList<StatusItem>? = null
)
@Parcelize
data class TicketDataItem(

	@field:SerializedName("note")
	val note: String? = null,

	@field:SerializedName("attachments")
	val attachments: ArrayList<String>? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("subject")
	val subject: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("ticket_id")
	val ticketId: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: String? = null
):Parcelable

data class Analytics(

	@field:SerializedName("close_ticket")
	val closeTicket: Int? = null,

	@field:SerializedName("open_ticket")
	val openTicket: Int? = null,

	@field:SerializedName("new_ticket")
	val newTicket: Int? = null
)

data class DataItem(

	@field:SerializedName("note")
	val note: Any? = null,

	@field:SerializedName("attachments")
	val attachments: ArrayList<String>? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("subject")
	val subject: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("ticket_id")
	val ticketId: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Ticket(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("data")
	val data: ArrayList<TicketDataItem>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: Any? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("links")
	val links: ArrayList<LinksItem>? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)
