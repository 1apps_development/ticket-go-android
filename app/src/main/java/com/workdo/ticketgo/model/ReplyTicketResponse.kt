package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class ReplyTicketResponse(

	@field:SerializedName("data")
	val data: ReplyData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class ReplyData(

	@field:SerializedName("replay")
	val replay: Replay? = null
)

data class Replay(

	@field:SerializedName("attachments")
	val attachments: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("sender")
	val sender: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("ticket_id")
	val ticketId: Int? = null
)
