package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class TicketSettingsResponse(

	@field:SerializedName("data")
	val data: TicketSettingsData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class TicketSettingsData(

	@field:SerializedName("custom_field")
	val customField: ArrayList<CustomFieldItem>? = null
)

data class CustomFieldItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("is_required")
	val isRequired: Int? = null,

	@field:SerializedName("custom_id")
	val customId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("width")
	val width: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("placeholder")
	val placeholder: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("created_by")
	val createdBy: String? = null,

	@field:SerializedName("order")
	val order: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
