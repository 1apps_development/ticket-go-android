package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class SiteSettingsResponse(

	@field:SerializedName("data")
	val data: SiteData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Site(

	@field:SerializedName("meta_image")
	val metaImage: String? = null,

	@field:SerializedName("PUSHER_APP_KEY")
	val pUSHERAPPKEY: String? = null,

	@field:SerializedName("strictly_cookie_description")
	val strictlyCookieDescription: String? = null,

	@field:SerializedName("wasabi_max_upload_size")
	val wasabiMaxUploadSize: String? = null,

	@field:SerializedName("necessary_cookies")
	val necessaryCookies: String? = null,

	@field:SerializedName("s3_bucket")
	val s3Bucket: String? = null,

	@field:SerializedName("cookie_title")
	val cookieTitle: String? = null,

	@field:SerializedName("enable_cookie")
	val enableCookie: String? = null,
	@field:SerializedName("gdpr_cookie")
	val gdprCookie: String? = null,

	@field:SerializedName("storage_setting")
	val storageSetting: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("knowledge")
	val knowledge: String? = null,

	@field:SerializedName("s3_region")
	val s3Region: String? = null,

	@field:SerializedName("PUSHER_APP_SECRET")
	val pUSHERAPPSECRET: String? = null,

	@field:SerializedName("CHAT_MODULE")
	val cHATMODULE: String? = null,

	@field:SerializedName("PUSHER_APP_ID")
	val pUSHERAPPID: String? = null,

	@field:SerializedName("wasabi_storage_validation")
	val wasabiStorageValidation: String? = null,

	@field:SerializedName("SITE_RTL")
	val sITERTL: String? = null,

	@field:SerializedName("s3_secret")
	val s3Secret: String? = null,

	@field:SerializedName("wasabi_url")
	val wasabiUrl: String? = null,

	@field:SerializedName("default_language")
	val defaultLanguage: String? = null,

	@field:SerializedName("wasabi_root")
	val wasabiRoot: String? = null,

	@field:SerializedName("meta_description")
	val metaDescription: String? = null,

	@field:SerializedName("cust_theme_bg")
	val custThemeBg: String? = null,

	@field:SerializedName("FOOTER_TEXT")
	val fOOTERTEXT: String? = null,

	@field:SerializedName("PUSHER_APP_CLUSTER")
	val pUSHERAPPCLUSTER: String? = null,

	@field:SerializedName("cookie_logging")
	val cookieLogging: String? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("display_landing")
	val displayLanding: String? = null,

	@field:SerializedName("meta_keywords")
	val metaKeywords: String? = null,

	@field:SerializedName("s3_max_upload_size")
	val s3MaxUploadSize: String? = null,

	@field:SerializedName("DEFAULT_LANG")
	val dEFAULTLANG: String? = null,

	@field:SerializedName("SIGNUP")
	val sIGNUP: String? = null,

	@field:SerializedName("wasabi_secret")
	val wasabiSecret: String? = null,

	@field:SerializedName("contactus_url")
	val contactusUrl: String? = null,

	@field:SerializedName("wasabi_region")
	val wasabiRegion: String? = null,

	@field:SerializedName("wasabi_key")
	val wasabiKey: String? = null,

	@field:SerializedName("s3_endpoint")
	val s3Endpoint: String? = null,

	@field:SerializedName("local_storage_max_upload_size")
	val localStorageMaxUploadSize: String? = null,

	@field:SerializedName("white_logo")
	val whiteLogo: String? = null,

	@field:SerializedName("favicon")
	val favicon: String? = null,

	@field:SerializedName("cust_darklayout")
	val custDarklayout: String? = null,

	@field:SerializedName("s3_url")
	val s3Url: String? = null,

	@field:SerializedName("wasabi_bucket")
	val wasabiBucket: String? = null,

	@field:SerializedName("cookie_description")
	val cookieDescription: String? = null,

	@field:SerializedName("app_name")
	val appName: String? = null,

	@field:SerializedName("local_storage_validation")
	val localStorageValidation: String? = null,

	@field:SerializedName("s3_key")
	val s3Key: String? = null,

	@field:SerializedName("strictly_cookie_title")
	val strictlyCookieTitle: String? = null,

	@field:SerializedName("more_information_description")
	val moreInformationDescription: String? = null,

	@field:SerializedName("s3_storage_validation")
	val s3StorageValidation: String? = null
)

data class SiteData(

	@field:SerializedName("site")
	val site: Site? = null
)
