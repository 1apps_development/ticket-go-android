package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class EmailSettingsPageResponse(

	@field:SerializedName("data")
	val data: EmailSiteData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class EmailSiteData(

	@field:SerializedName("site")
	val site: EmailSite? = null
)

data class EmailSite(

	@field:SerializedName("mail_driver")
	val mailDriver: String? = null,

	@field:SerializedName("mail_encryption")
	val mailEncryption: Any? = null,

	@field:SerializedName("mail_from_name")
	val mailFromName: Any? = null,

	@field:SerializedName("mail_password")
	val mailPassword: Any? = null,

	@field:SerializedName("mail_port")
	val mailPort: String? = null,

	@field:SerializedName("mail_host")
	val mailHost: String? = null,

	@field:SerializedName("mail_from_address")
	val mailFromAddress: Any? = null,

	@field:SerializedName("mail_username")
	val mailUsername: Any? = null
)
