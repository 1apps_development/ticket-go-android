package com.workdo.ticketgo.model

data class AnalyticsData(val id:Int,val color:String,val percentage:String,val name:String)
