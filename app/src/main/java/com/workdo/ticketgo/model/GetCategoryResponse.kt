package com.workdo.ticketgo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class GetCategoryResponse(

	@field:SerializedName("data")
	val data: GetCategoryData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
@Parcelize
data class GetCategoryData(

	@field:SerializedName("category")
	val category: ArrayList<CategoryDataItem>? = null
):Parcelable
@Parcelize
data class CategoryDataItem(

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
):Parcelable
