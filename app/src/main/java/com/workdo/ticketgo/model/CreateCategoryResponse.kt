package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class CreateCategoryResponse(

	@field:SerializedName("data")
	val data: CreateCategoryData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CreateCategoryData(

	@field:SerializedName("category")
	val category: Category? = null
)

data class Category(

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("created_by")
	val createdBy: String? = null
)
