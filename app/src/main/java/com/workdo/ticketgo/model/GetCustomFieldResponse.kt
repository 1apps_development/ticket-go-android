package com.workdo.ticketgo.model

import com.google.gson.annotations.SerializedName

data class GetCustomFieldResponse(

	@field:SerializedName("“message”")
	val message: String? = null,

	@field:SerializedName("“status”")
	val status: Int? = null,

	@field:SerializedName("“data”")
	val data: CustomFieldData? = null
)

data class CustomFieldsItem(

	@field:SerializedName("“type”")
	val type: String? = null,

	@field:SerializedName("“name”")
	val name: String? = null,

	@field:SerializedName("“placeholder”")
	val placeholder: String? = null,

	@field:SerializedName("“id”")
	val id: Int? = null
)

data class CustomFieldData(

	@field:SerializedName("“custom_fields”")
	val customFields: ArrayList<CustomFieldsItem>? = null
)
