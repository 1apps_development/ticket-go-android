package com.workdo.ticketgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.FaqListAdapter
import com.workdo.ticketgo.base.BaseActivity
import com.workdo.ticketgo.databinding.ActFaqBinding
import com.workdo.ticketgo.databinding.NewFaqDialogBinding
import com.workdo.ticketgo.model.FaqDataItem
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.*
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import com.workdo.ticketgo.util.ExtensionFunctions.show
import kotlinx.coroutines.launch

class ActFaq : BaseActivity() {
    private var _binding: ActFaqBinding? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var isLoading = false
    private var isLastPage = false
    private var currentPage = 1
    private var totalPages: Int = 0
    private var faqAdapter: FaqListAdapter? = null
    private val binding get() = _binding!!

    private var faqList=ArrayList<FaqDataItem>()

    override fun setLayout(): View = binding.root

    override fun initView() {
        _binding = ActFaqBinding.inflate(layoutInflater)
        init()
    }
    private fun init()
    {
        initClickListener()
        linearLayoutManager= LinearLayoutManager(this@ActFaq, RecyclerView.VERTICAL,false)

        pagination()

        faqAdapter = FaqListAdapter(faqList) { pos, type ->
            if (type == Constants.DELETE) {
                callDeleteFaq(pos)
            }else if(type==Constants.UPDATE)
            {
                newFaqDialog(Constants.UPDATE,faqList[pos])
            }
        }
        setupAdapter()

        callGetFaq()
    }

    private fun setupAdapter() {
        binding.rvFaq.apply {
            layoutManager = linearLayoutManager
            adapter = faqAdapter

        }
    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callGetFaq()


            }
        }
        binding.rvFaq.addOnScrollListener(paginationListener)
    }

    private fun initClickListener()
    {

        binding.btnAddNewQuestion.setOnClickListener {
            newFaqDialog(Constants.CREATE, FaqDataItem())

        }

        binding.ivBack.setOnClickListener {
            finish()
        }
    }


    private fun callGetFaq() {
        val request = HashMap<String, String>()
        request["user_id"] = SharePreference.getStringPref(this@ActFaq, SharePreference.userId) ?: ""
        request["search"] = ""

        Utils.showLoadingProgress(this@ActFaq)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActFaq).faqListApi(currentPage.toString(), request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            currentPage = response.body.data?.faq?.currentPage ?:0
                            totalPages = response.body.data?.faq?.lastPage?:0
                            response.body.data?.faq?.data?.let { faqList.addAll(it) }


                            if (currentPage >= totalPages) {
                                isLastPage = true
                            }
                            isLoading = false

                            if(faqList.size==0)
                            {
                                binding.rvFaq.hide()
                                binding.tvNoDataFound.show()
                            }else
                            {
                                binding.rvFaq.show()
                                binding.tvNoDataFound.hide()

                            }

                            faqAdapter?.notifyDataSetChanged()

                        }
                        0 -> {
                            Utils.errorAlert(this@ActFaq, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(this@ActFaq, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActFaq, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFaq,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFaq, "Something went wrong")
                }
            }
        }
    }



    private fun callDeleteFaq(pos: Int) {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(this@ActFaq, SharePreference.userId) ?: ""

        Utils.showLoadingProgress(this@ActFaq)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActFaq).deleteFaq(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            faqList.removeAt(pos)
                            faqAdapter?.notifyDataSetChanged()

                            if(faqList.size==0)
                            {
                                binding.rvFaq.hide()
                                binding.tvNoDataFound.show()
                            }else
                            {
                                binding.rvFaq.show()
                                binding.tvNoDataFound.hide()
                            }

                        }
                        0 -> {
                            Utils.errorAlert(this@ActFaq, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(this@ActFaq, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActFaq, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFaq,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFaq, "Something went wrong")
                }
            }
        }
    }

    private fun callCreateFaq(request:HashMap<String,String>,dialog: BottomSheetDialog) {


        Utils.showLoadingProgress(this@ActFaq)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActFaq).createFaq(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            dialog.dismiss()
                            currentPage=1
                            isLoading=true
                            isLastPage=false
                            faqList.clear()
                            callGetFaq()

                        }
                        0 -> {
                            Utils.errorAlert(this@ActFaq, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActFaq, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActFaq, response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFaq,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFaq, "Something went wrong")
                }
            }
        }
    }




    private fun newFaqDialog(type:String,data:FaqDataItem) {
        val bottomSheetDialog = BottomSheetDialog(this@ActFaq, R.style.BottomSheetDialogTheme)
        val bottomSheetBinding = NewFaqDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        if(type==Constants.UPDATE)
        {
            bottomSheetBinding.tvTitle.text=resources.getString(R.string.edit_)
            bottomSheetBinding.tvSubTitle.text=resources.getString(R.string.update_faq_)
            bottomSheetBinding.btnCreateFaq.text = resources.getString(R.string.update_faq)
            bottomSheetBinding.edQuestion.setText(data.title.toString())
            bottomSheetBinding.edDescription.setText(data.description.toString())
        }

        bottomSheetBinding.btnCreateFaq.setOnClickListener {
            if(bottomSheetBinding.edQuestion.text?.isEmpty() == true)
            {
                Utils.errorAlert(this@ActFaq,"Question Field Should Not Be Empty")
            }else if(bottomSheetBinding.edDescription.text?.isEmpty()==true)
            {
                Utils.errorAlert(this@ActFaq,"Answer Field Should Not Be Empty")
            }else
            {
                if(type == Constants.CREATE)
                {
                    val request=HashMap<String,String>()
                    request["title"]=bottomSheetBinding.edQuestion.text.toString()
                    request["description"]=bottomSheetBinding.edDescription.text.toString()
                    callCreateFaq(request,bottomSheetDialog)
                }else
                {
                    val request=HashMap<String,String>()
                    request["id"]=data.id.toString()
                    request["title"]=bottomSheetBinding.edQuestion.text.toString()
                    request["description"]=bottomSheetBinding.edDescription.text.toString()
                    callCreateFaq(request,bottomSheetDialog)
                }
            }
        }


        bottomSheetDialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}