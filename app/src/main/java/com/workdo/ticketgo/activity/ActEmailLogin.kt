package com.workdo.ticketgo.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.workdo.ticketgo.MainActivity
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.ActEmailLoginBinding
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.ExtensionFunctions.isEnable
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch


class ActEmailLogin : AppCompatActivity() {

    private  var _binding: ActEmailLoginBinding?=null

    private val binding get() = _binding!!

    var token=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActEmailLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FirebaseApp.initializeApp(this@ActEmailLogin)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    println("Failed to get token")
                    return@OnCompleteListener
                }
                token = task.result.toString()
            })
        initClickListeners()

       if(Constants.IsDemoMode)
       {
           binding.edEmailAddress.setText("android@example.com")
           binding.edPassword.setText("1234")

           binding.edEmailAddress.isEnabled=false
           binding.edPassword.isEnabled=false
       }

    }


    private fun callLoginApi(loginRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActEmailLogin)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActEmailLogin).getLogin(loginRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loginResponse = response.body.data

                    when (response.body.status) {
                        1 -> {

                            SharePreference.setBooleanPref(this@ActEmailLogin, SharePreference.isLogin, true)
                            SharePreference.setStringPref(this@ActEmailLogin, SharePreference.userId, loginResponse?.id.toString())
                            SharePreference.setStringPref(this@ActEmailLogin, SharePreference.userName, loginResponse?.name.toString())
                            SharePreference.setStringPref(this@ActEmailLogin, SharePreference.userProfile, loginResponse?.imageUrl.toString())

                            SharePreference.setStringPref(this@ActEmailLogin, SharePreference.userEmail, loginResponse?.email.toString())

                            SharePreference.setStringPref(this@ActEmailLogin, SharePreference.token, loginResponse?.accessToken.toString())
                            startActivity(Intent(this@ActEmailLogin, MainActivity::class.java))
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(this@ActEmailLogin, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailLogin, response.body.data?.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailLogin, resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailLogin, "Something went wrong")
                }
            }
        }
    }


    private fun initClickListeners() {
        binding.btnLogin.setOnClickListener {

            if(binding.edEmailAddress.text?.isEmpty() == true)
            {
                Utils.errorAlert(this@ActEmailLogin,resources.getString(R.string.please_enter_email))
            }else if(!Utils.isValidEmail(binding.edEmailAddress.text.toString()))
            {
                Utils.errorAlert(this@ActEmailLogin,resources.getString(R.string.enter_valid_email))

            }
            else if(binding.edPassword.text?.isEmpty()==true)
            {
                Utils.errorAlert(this@ActEmailLogin,resources.getString(R.string.please_enter_password))

            }else
            {
                val request=HashMap<String,String>()
                request["email"]=binding.edEmailAddress.text.toString()
                request["password"]=binding.edPassword.text.toString()
                request["token"]=token
                request["device_type"]="android"
                callLoginApi(request)
            }

        }


    }

    override fun onDestroy() {
        super.onDestroy()
        Utils.dismissLoadingProgress()
        _binding=null
    }
}