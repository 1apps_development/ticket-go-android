package com.workdo.ticketgo.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.ticketgo.databinding.ActIntroBinding
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.SharePreference

class ActIntro : AppCompatActivity() {
    private  var _binding: ActIntroBinding?=null
    private val binding get()=_binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActIntroBinding.inflate(layoutInflater)
        setContentView(_binding?.root)
        binding.edBaseUrl.setText(Constants.BASE_URL)



        if(Constants.IsDemoMode)
        {
            binding.edBaseUrl.isEnabled=false
            binding.edBaseUrl.isClickable=false
        }


        binding.constraintLogin.setOnClickListener {
            SharePreference.setStringPref(this@ActIntro,SharePreference.BaseUrl,binding.edBaseUrl.text.toString().plus("/"))

            startActivity(Intent(this@ActIntro, ActEmailLogin::class.java))
        }



    }

    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}