package com.workdo.ticketgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.TicketFieldListAdapter
import com.workdo.ticketgo.databinding.ActTicketFieldsBinding
import com.workdo.ticketgo.databinding.NewFieldDialogBinding
import com.workdo.ticketgo.model.CustomFieldItem
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import com.workdo.ticketgo.util.ExtensionFunctions.show
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch

class ActTicketFields : AppCompatActivity() {
    private var _binding: ActTicketFieldsBinding? = null
    private val ticketFieldSList=ArrayList<CustomFieldItem>()
    private lateinit var ticketListAdapter:TicketFieldListAdapter
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActTicketFieldsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.btnCreateField.setOnClickListener {
            newFieldDialog(Constants.CREATE,"","","")
        }
        ticketListAdapter= TicketFieldListAdapter(ticketFieldSList){pos,type ->

            if(type==Constants.DELETE)
            {
                deleteTicketField(ticketFieldSList[pos].id.toString(),pos)
            }else if(type==Constants.UPDATE)
            {
                newFieldDialog(Constants.UPDATE,ticketFieldSList[pos].id.toString(),ticketFieldSList[pos].name.toString(),ticketFieldSList[pos].placeholder.toString())
            }

        }

        setupAdapter()
        callGetTicketFieldList()
    }


    private fun callGetTicketFieldList() {
        val request = HashMap<String, String>()
        request["user_id"] =
            SharePreference.getStringPref(this@ActTicketFields, SharePreference.userId) ?: ""
        Utils.showLoadingProgress(this@ActTicketFields)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActTicketFields).ticketSettings(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.customField?.let { ticketFieldSList.addAll(it) }
                            ticketListAdapter.notifyDataSetChanged()

                            if(ticketFieldSList.size==0)
                            {
                                binding.rvTicketFieldList.hide()
                                binding.tvNoDataFound.show()
                            }else
                            {
                                binding.tvNoDataFound.hide()
                                binding.rvTicketFieldList.show()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(this@ActTicketFields, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(this@ActTicketFields, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActTicketFields, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActTicketFields,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActTicketFields, "Something went wrong")
                }
            }
        }
    }
    private fun newFieldDialog(type:String,id: String,label:String,placeHolder:String) {
        val bottomSheetDialog = BottomSheetDialog(this@ActTicketFields, R.style.BottomSheetDialogTheme)
        val bottomSheetBinding = NewFieldDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        if(type==Constants.UPDATE)
        {
            bottomSheetBinding.tvNewCategoryTitle.text="Update TicketField"
            bottomSheetBinding.tvCreate.text="Update"
            bottomSheetBinding.edLabelName.setText(label)
            bottomSheetBinding.edPlaceHolderName.setText(placeHolder)

        }


        bottomSheetBinding.btnCreate.setOnClickListener {
            if(bottomSheetBinding.edLabelName.text?.isEmpty() == true)
            {
                Utils.errorAlert(this@ActTicketFields,"Label Name is Required")
            }else if(bottomSheetBinding.edPlaceHolderName.text?.isEmpty()==true)
            {
                Utils.errorAlert(this@ActTicketFields,"PlaceHolder Name is Required")
            }else
            {
                val request=HashMap<String,String>()
                if(type==Constants.UPDATE)
                {
                    request["id"]=id
                    request["user_id"]=SharePreference.getStringPref(this@ActTicketFields,SharePreference.userId)?:""
                    request["name"]=bottomSheetBinding.edLabelName.text.toString()
                    request["placeholder"]=bottomSheetBinding.edPlaceHolderName.text.toString()
                }else
                {
                    request["user_id"]=SharePreference.getStringPref(this@ActTicketFields,SharePreference.userId)?:""
                    request["name"]=bottomSheetBinding.edLabelName.text.toString()
                    request["placeholder"]=bottomSheetBinding.edPlaceHolderName.text.toString()
                }

                addCustomField(request)
                bottomSheetDialog.dismiss()

            }
        }


        bottomSheetDialog.show()
    }

    private fun addCustomField(request:HashMap<String,String>) {

        Utils.showLoadingProgress(this@ActTicketFields)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActTicketFields).addCustomField(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            val data=response.body.data?.customField
                            ticketFieldSList.add(CustomFieldItem(data?.updatedAt.toString(),
                                name = data?.name.toString(),
                                placeholder = data?.placeholder.toString(),
                                createdBy = data?.createdBy.toString(),
                                id = data?.id,
                                createdAt = data?.createdAt.toString()))
                            ticketListAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(this@ActTicketFields, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActTicketFields, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActTicketFields, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActTicketFields,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActTicketFields, "Something went wrong")
                }
            }
        }
    }





    private fun deleteTicketField(id:String,pos:Int) {
        val request = HashMap<String, String>()
        request["id"] = id
        Utils.showLoadingProgress(this@ActTicketFields)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActTicketFields).deleteCustomField(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            ticketFieldSList.removeAt(pos)
                            ticketListAdapter.notifyDataSetChanged()

                            if(ticketFieldSList.size==0)
                            {
                                binding.rvTicketFieldList.hide()
                                binding.tvNoDataFound.show()
                            }else
                            {
                                binding.tvNoDataFound.hide()
                                binding.rvTicketFieldList.show()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(this@ActTicketFields, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActTicketFields, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActTicketFields, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActTicketFields,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActTicketFields, "Something went wrong")
                }
            }
        }
    }

    private fun setupAdapter() {
        binding.rvTicketFieldList.apply {
            layoutManager = LinearLayoutManager(this@ActTicketFields)
            adapter = ticketListAdapter
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}