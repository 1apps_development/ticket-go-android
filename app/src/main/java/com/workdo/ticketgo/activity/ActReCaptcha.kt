package com.workdo.ticketgo.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.ActRecaptchaBinding
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch

class ActReCaptcha : AppCompatActivity() {
    private var _binding: ActRecaptchaBinding? = null
    private var captchaStatus = "off"
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActRecaptchaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.swRecaptcha.setOnCheckedChangeListener { buttonView, isChecked ->
            captchaStatus = if (isChecked) {
                "on"
            } else {
                "off"
            }

        }

        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.btnUpdateRecaptcha.setOnClickListener {
            if (binding.edGoogleReCaptchaKey.text?.isEmpty() == true) {
                Utils.errorAlert(this@ActReCaptcha, "captcha key is required")
            } else if (binding.edGoogleReCaptchaSecretKey.text?.isEmpty() == true) {
                Utils.errorAlert(this@ActReCaptcha, "captcha secret is required")

            } else {
                callReCaptchaApiCall()
            }

        }
    }

    private fun callReCaptchaApiCall() {
        val request = HashMap<String, String>()
        request["id"] = SharePreference.getStringPref(this@ActReCaptcha, SharePreference.userId) ?: ""
        request["recaptcha_module"] = captchaStatus
        request["google_recaptcha_secret"] = binding.edGoogleReCaptchaSecretKey.text.toString()
        request["google_recaptcha_key"] = binding.edGoogleReCaptchaKey.text.toString()
        Utils.showLoadingProgress(this@ActReCaptcha)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActReCaptcha).reCaptchaSettings(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(this@ActReCaptcha, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActReCaptcha, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActReCaptcha, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActReCaptcha,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActReCaptcha, "Something went wrong")
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
           _binding=null

    }
}