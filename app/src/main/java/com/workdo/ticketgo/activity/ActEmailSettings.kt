package com.workdo.ticketgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.ActEmailSettingsBinding
import com.workdo.ticketgo.model.EmailSiteData
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch

class ActEmailSettings : AppCompatActivity() {
    private  var _binding:ActEmailSettingsBinding?=null
    private val binding get()=_binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding= ActEmailSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
        getSettingPage()

        binding.btnTestMail.setOnClickListener {

            if(isValid())
            {
                sendEmailApi()
            }

        }


        binding.btnSaveChanges.setOnClickListener {

            if(isValid())
            {
                saveEmailSetting()
            }
        }
    }


    private fun isValid():Boolean
    {
        if(binding.edMailDriver.text?.isEmpty() == true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Mail Driver  is required")
            return false
        }else if(binding.edUserName.text?.isEmpty()==true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Username is required")
            return false


        }else if(binding.edMailFromAddress.text?.isEmpty()==true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Parent Mail Sender  is required")
            return false


        }else if(binding.edMailPassword.text?.isEmpty()==true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Password  is required")
            return false


        }else if(binding.edMailFromName.text?.isEmpty()==true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Sender Name is required")
            return false


        }else if(binding.edMailPort.text?.isEmpty()==true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Mail Port  is required")
            return false


        }else if(binding.edMailEncryption.text?.isEmpty()==true)
        {
            Utils.errorAlert(this@ActEmailSettings,"Mail Encryption  is required")
            return false


        }
        return true
    }


    private fun sendEmailApi() {
        val request=HashMap<String,String>()
        request["id"]= SharePreference.getStringPref(this@ActEmailSettings,SharePreference.userId)?:""
        request["user_mail"]= SharePreference.getStringPref(this@ActEmailSettings,SharePreference.userEmail).toString()
        request["mail_driver"]= binding.edMailDriver.text.toString()
        request["mail_host"]= binding.edMailHost.text.toString()
        request["mail_port"]= binding.edMailPort.text.toString()
        request["mail_username"]= binding.edUserName.text.toString()
        request["mail_password"]= binding.edMailPassword.text.toString()
        request["mail_encryption"]= binding.edMailEncryption.text.toString()
        request["mail_from_address"]= binding.edMailFromAddress.text.toString()
        request["mail_from_name"]= binding.edMailFromAddress.text.toString()
        Utils.showLoadingProgress(this@ActEmailSettings)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActEmailSettings).sendTestEmail(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                        }
                        0 -> {
                            Utils.errorAlert(this@ActEmailSettings, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActEmailSettings, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActEmailSettings, response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailSettings,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailSettings, "Something went wrong")
                }
            }
        }
    }

    private fun getSettingPage() {
        val request=HashMap<String,String>()
        request["id"]= SharePreference.getStringPref(this@ActEmailSettings,SharePreference.userId)?:""

        Utils.showLoadingProgress(this@ActEmailSettings)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActEmailSettings).emailSettingPage(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            response.body.data?.let { setupData(it) }

                        }
                        0 -> {
                            Utils.errorAlert(this@ActEmailSettings, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(this@ActEmailSettings, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActEmailSettings, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailSettings,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailSettings, "Something went wrong")
                }
            }
        }
    }
    private fun setupData(data:EmailSiteData)
    {
        binding.edMailDriver.setText(data.site?.mailDriver.toString())
        binding.edMailEncryption.setText(data.site?.mailEncryption.toString())
        binding.edMailPort.setText(data.site?.mailPort.toString())

        binding.edMailHost.setText(data.site?.mailHost.toString())
        binding.edMailFromAddress.setText(data.site?.mailFromAddress.toString())
        binding.edMailFromName.setText(data.site?.mailFromName.toString())
        binding.edMailPassword.setText(data.site?.mailPassword.toString())
        binding.edUserName.setText(data.site?.mailUsername.toString())
    }




    private fun saveEmailSetting() {
        val request=HashMap<String,String>()
        request["id"]= SharePreference.getStringPref(this@ActEmailSettings,SharePreference.userId)?:""
        request["user_mail"]= SharePreference.getStringPref(this@ActEmailSettings,SharePreference.userEmail).toString()
        request["mail_driver"]= binding.edMailDriver.text.toString()
        request["mail_host"]= binding.edMailHost.text.toString()
        request["mail_port"]= binding.edMailPort.text.toString()
        request["mail_username"]= binding.edUserName.text.toString()
        request["mail_password"]= binding.edMailPassword.text.toString()
        request["mail_encryption"]= binding.edMailEncryption.text.toString()
        request["mail_from_address"]= binding.edMailFromAddress.text.toString()
        request["mail_from_name"]= binding.edMailFromAddress.text.toString()
        Utils.showLoadingProgress(this@ActEmailSettings)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActEmailSettings).emailSetting(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {



                        }
                        0 -> {
                            Utils.errorAlert(this@ActEmailSettings, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(this@ActEmailSettings, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActEmailSettings, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailSettings,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailSettings, "Something went wrong")
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}