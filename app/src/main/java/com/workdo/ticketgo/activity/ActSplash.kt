package com.workdo.ticketgo.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.workdo.ticketgo.MainActivity
import com.workdo.ticketgo.R
import com.workdo.ticketgo.util.SharePreference


class ActSplash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val userId =
            SharePreference.getStringPref(this@ActSplash, SharePreference.userId).toString()



        setContentView(R.layout.act_splash)
        Handler(Looper.getMainLooper()).postDelayed({

            if (userId.isNotEmpty()) {
                val intent = Intent(this@ActSplash, MainActivity::class.java)
                startActivity(intent)
            }else
            {
                startActivity(Intent(this@ActSplash, ActIntro::class.java))

            }
            finish()
        }, 2000)

    }

}