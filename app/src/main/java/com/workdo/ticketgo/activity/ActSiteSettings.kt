package com.workdo.ticketgo.activity

import android.app.Activity
import android.content.Intent
import android.os.Environment
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.ticketgo.R
import com.workdo.ticketgo.base.BaseActivity
import com.workdo.ticketgo.databinding.ActSiteSettingsBinding
import com.workdo.ticketgo.databinding.NewFieldDialogBinding
import com.workdo.ticketgo.model.Site
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.ExtensionFunctions.loadFile
import com.workdo.ticketgo.util.ExtensionFunctions.loadUrl
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch
import java.io.File

class ActSiteSettings : BaseActivity() {
    private var _binding: ActSiteSettingsBinding? = null
    private var imageFile: File? = null
    private var imageWhiteLogo: File? = null
    private var logoFile: File? = null
    var defaultLanguage = ""
    var isFirstTime=true
    private val binding get() = _binding!!


    override fun setLayout(): View = binding.root

    override fun initView() {
        _binding = ActSiteSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }

        siteSettingsApi()


        binding.constraintChooseFav.setOnClickListener {
            ImagePicker.with(this@ActSiteSettings).cropSquare().compress(1024).saveDir(File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                resources.getString(R.string.app_name))).maxResultSize(1080, 1080)
                .createIntent { intent ->

                    startActivityForResult(intent, 101)
                }
        }

        binding.constraintLogoChoose.setOnClickListener {
            ImagePicker.with(this@ActSiteSettings).cropSquare().compress(1024).saveDir(File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                resources.getString(R.string.app_name))).maxResultSize(1080, 1080)
                .createIntent { intent ->
                    startActivityForResult(intent, 201)

                }
        }

        binding.constraintWhiteChoose.setOnClickListener {
            ImagePicker.with(this@ActSiteSettings).cropSquare().compress(1024).saveDir(File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                resources.getString(R.string.app_name))).maxResultSize(1080, 1080)
                .createIntent { intent ->
                    startActivityForResult(intent, 301)

                }
        }



        binding.btnSaveChanges.setOnClickListener {
            if(imageFile==null)
            {
                Utils.errorAlert(this@ActSiteSettings,"FavIcon is required")
            }else if(logoFile==null)
            {
                Utils.errorAlert(this@ActSiteSettings,"Logo is required")

            }else if(imageWhiteLogo==null)
            {
                Utils.errorAlert(this@ActSiteSettings,"WhiteLogo is required")

            }else if(binding.edAppName.text?.isEmpty()==true)
            {
                Utils.errorAlert(this@ActSiteSettings,"App Name is required")

            }else if(binding.edFooterText.text?.isEmpty()==true)

            {
                Utils.errorAlert(this@ActSiteSettings,"Footer text is required")

            }else if(binding.edGdprDescription.text?.isEmpty()==true)
            {
                Utils.errorAlert(this@ActSiteSettings,"Gdpr text is required")

            }else
            {
                updateSiteSettingsApi()
            }
        }


    }


    private fun siteSettingsApi() {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(this@ActSiteSettings, SharePreference.userId) ?: ""
        Utils.showLoadingProgress(this@ActSiteSettings)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSiteSettings).siteSettings(request)) {
                is NetworkResponse.Success -> {

                    when (response.body.status) {
                        1 -> {

                            response.body.data?.site?.let { setupSiteData(it) }
                            getLanguages(response.body.data?.site?.dEFAULTLANG.toString())

                        }
                        0 -> {
                            Utils.dismissLoadingProgress()
                            Utils.errorAlert(this@ActSiteSettings, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                            Utils.errorAlert(this@ActSiteSettings, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActSiteSettings, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSiteSettings,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSiteSettings, "Something went wrong")
                }
            }
        }
    }

    private fun getLanguages(defaultLanguage:String) {

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSiteSettings).languages()) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.language?.let { setLanguageAdapter(it,defaultLanguage) }
                        }
                        0 -> {
                            Utils.errorAlert(this@ActSiteSettings, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActSiteSettings, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActSiteSettings, response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSiteSettings,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSiteSettings, "Something went wrong")
                }
            }
        }
    }

    private fun setLanguageAdapter(languageList: ArrayList<String>,defaultLanguage:String) {

        val languageAdapter =
            ArrayAdapter(this@ActSiteSettings, R.layout.item_dropdown_menu, languageList)


        binding.spLanguage.adapter = languageAdapter


        binding.spLanguage.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

                    if(isFirstTime)
                    {
                        if(defaultLanguage.isNotEmpty())
                        {
                            for(i in 0 until languageList.size)
                            {
                                if(defaultLanguage==languageList[i])
                                {
                                    binding.spLanguage.setSelection(i)
                                }
                            }


                        }else
                        {
                            binding.spLanguage.setSelection(p2)

                        }
                    }else
                    {
                        binding.spLanguage.setSelection(p2)

                    }

                    isFirstTime=false

                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
    }


    private fun updateSiteSettingsApi() {
        val swRtl = if (binding.swRtl.isChecked) {
            "on"
        } else {
            "off"
        }

        val gdprCookies = if (binding.swGdpr.isChecked) {
            "on"
        } else {
            "off"
        }

        val id = Utils.setRequestBody(SharePreference.getStringPref(this@ActSiteSettings,
            SharePreference.userId) ?: "")
        val appName = Utils.setRequestBody(binding.edAppName.text.toString())
        val cookieText = Utils.setRequestBody(binding.edGdprDescription.text.toString())
        val cookieSwitch = Utils.setRequestBody(gdprCookies)
        val rtlSwitch = Utils.setRequestBody(swRtl)
        val footerText = Utils.setRequestBody(binding.edFooterText.text.toString())
        val defaultLanguage = Utils.setRequestBody(binding.spLanguage.selectedItem.toString())

        val favIcon = imageFile?.let {
            Utils.setImageUpload("favicon", it)
        }
        val logoFile = logoFile?.let {
            Utils.setImageUpload("logo", it)
        }
        val whiteLogo = imageWhiteLogo?.let {
            Utils.setImageUpload("white_logo", it)
        }

        Utils.showLoadingProgress(this@ActSiteSettings)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSiteSettings).siteSettingPage(id, appName, defaultLanguage, rtlSwitch, cookieSwitch, cookieText,
                    footerText, favIcon, logoFile, whiteLogo)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            finish()

                        }
                        0 -> {
                            Utils.errorAlert(this@ActSiteSettings, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(this@ActSiteSettings, response.body.message.toString())
                    } else {
                        Utils.errorAlert(this@ActSiteSettings, response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSiteSettings,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSiteSettings, "Something went wrong")
                }
            }
        }
    }


    private fun setupSiteData(siteData: Site) {
        val baseUrl=SharePreference.getStringPref(this@ActSiteSettings,SharePreference.BaseUrl)
        binding.ivFavPlaceHolder.loadUrl(baseUrl.plus(siteData.favicon))
        binding.ivWhiteLogo.loadUrl(baseUrl.plus(siteData.whiteLogo))
        binding.ivAppLogo.loadUrl(baseUrl.plus(siteData.logo))

        binding.edAppName.setText(siteData.appName)
        binding.edFooterText.setText(siteData.fOOTERTEXT)
        binding.edGdprDescription.setText(siteData.cookieDescription)

        binding.swRtl.isChecked = siteData.sITERTL == "on"
        binding.swGdpr.isChecked = siteData.gdprCookie == "on"


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val fileUri = data?.data!!

            when (requestCode) {
                101 -> {
                    fileUri.path.let {
                        imageFile = File(it.toString())
                    }
                    binding.ivFavPlaceHolder.loadFile(imageFile)
                }
                201 -> {
                    fileUri.path.let {
                        logoFile = File(it.toString())
                    }
                    binding.ivAppLogo.loadFile(logoFile)

                }
                301 -> {
                    fileUri.path.let {
                        imageWhiteLogo = File(it.toString())
                    }
                    binding.ivWhiteLogo.loadFile(imageWhiteLogo)

                }
            }

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}