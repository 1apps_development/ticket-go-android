package com.workdo.ticketgo.adapter

import android.app.Activity
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellTicketBinding
import com.workdo.ticketgo.model.TicketDataItem
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.DeleteTicketCallBack
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import com.workdo.ticketgo.util.ExtensionFunctions.show
import com.workdo.ticketgo.util.Utils

class TicketsAdapter(
    private val ticketList: ArrayList<TicketDataItem>,

    private val listener: (TicketDataItem, String, Int) -> Unit,
) :
    RecyclerView.Adapter<TicketsAdapter.TicketsViewHolder>() {
    inner class TicketsViewHolder(private val itemBinding: CellTicketBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: TicketDataItem, pos: Int) = with(itemBinding)
        {
            itemBinding.apply {
                tvTicketId.text = data.ticketId.toString()
                tvTicketName.text = data.subject.toString()
                tvUsername.text = data.name.toString()
                tvEmail.text = data.email.toString()
                tvStatus.text = data.status.toString()

                when (data.status) {
                    "Closed" -> {
                        tvStatus.setBackgroundColor(Color.parseColor("#336FD943"))
                        tvStatus.setTextColor(Color.parseColor("#6FD943"))
                        tvStatus.backgroundTintList =
                            ColorStateList.valueOf(Color.parseColor("#336FD943"))
                        clTicketView.background = ResourcesCompat.getDrawable(itemView.resources,
                            R.drawable.bg_closed,
                            null)
                        tvTitle.setTextColor(Color.parseColor("#FE007A"))
                        tvStatus.icon=ResourcesCompat.getDrawable(itemView.resources,R.drawable.ic_check_mark,null)
                        tvStatus.iconTint= ColorStateList.valueOf(Color.parseColor("#6FD943"))
                    }
                    "On Hold" -> {
                        tvStatus.setBackgroundColor(Color.parseColor("#33FC275A"))
                        tvStatus.setTextColor(Color.parseColor("#FC275A"))
                        tvStatus.backgroundTintList =
                            ColorStateList.valueOf(Color.parseColor("#33FC275A"))
                        clTicketView.background = ResourcesCompat.getDrawable(itemView.resources,
                            R.drawable.bg_on_hold,
                            null)
                        tvTitle.setTextColor(Color.parseColor("#9611E7"))
                        tvStatus.icon=ResourcesCompat.getDrawable(itemView.resources,R.drawable.ic_close_shape,null)
                        tvStatus.iconTint= ColorStateList.valueOf(Color.parseColor("#FC275A"))


                    }
                    "In Progress" -> {
                        tvStatus.setBackgroundColor(Color.parseColor("#33FFC700"))
                        tvStatus.setTextColor(Color.parseColor("#FFC700"))
                        tvStatus.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#33FFC700"))
                        clTicketView.background = ResourcesCompat.getDrawable(itemView.resources, R.drawable.round_light_gray_13, null)
                        tvTitle.setTextColor(Color.parseColor("#009CFE"))
                        tvStatus.icon=ResourcesCompat.getDrawable(itemView.resources,R.drawable.ic_line,null)
                        tvStatus.iconTint= ColorStateList.valueOf(Color.parseColor("#FFC700"))


                    }
                    else -> {
                        tvStatus.setBackgroundColor(Color.parseColor("#33FFC700"))
                        tvStatus.setTextColor(Color.parseColor("#FFC700"))
                        tvStatus.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#33FFC700"))
                        clTicketView.background = ResourcesCompat.getDrawable(itemView.resources, R.drawable.round_light_gray_13, null)
                        tvTitle.setTextColor(Color.parseColor("#009CFE"))

                    }
                }


                if (data.category?.isNotEmpty() == true) {
                    tvTitle.show()
                } else {
                    tvTitle.hide()

                }
                tvTitle.text = data.category.toString()

            }



            linearDelete.setOnClickListener {
                if (Constants.IsDemoMode) {
                    Utils.errorAlert(itemView.context as Activity, itemView.resources.getString(R.string.demo_user_message))
                } else {
                    listener(data, Constants.DELETE, pos)
                }

            }
            linearReply.setOnClickListener {
                if (Constants.IsDemoMode) {
                    Utils.errorAlert(itemView.context as Activity, itemView.resources.getString(R.string.demo_user_message))
                } else {
                    listener(data, Constants.REPLY, pos)
                }

            }


            clTicket.setOnClickListener {
                listener(data, Constants.ItemClick, pos)

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketsViewHolder {
        val view = CellTicketBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TicketsViewHolder(view)
    }

    override fun onBindViewHolder(holder: TicketsViewHolder, position: Int) {
        holder.bindItems(ticketList[position], position)
    }

    override fun getItemCount(): Int {
        return ticketList.size
    }


}