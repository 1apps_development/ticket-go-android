package com.workdo.ticketgo.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellFaqBinding
import com.workdo.ticketgo.model.FaqData
import com.workdo.ticketgo.model.FaqDataItem
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.Utils

class FaqListAdapter(private val faqList: ArrayList<FaqDataItem>, var listener: (Int, String) -> Unit) :
    RecyclerView.Adapter<FaqListAdapter.FaqViewHolder>() {

    inner class FaqViewHolder(private val binding: CellFaqBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItems(data: FaqDataItem, pos: Int)= with(binding) {
            binding.tvCount.text=data.id.toString()
            binding.tvFaqDescription.text=data.description.toString()
            binding.tvName.text=data.title.toString()



            binding.ivFilter.setOnClickListener {
                listener(pos,Constants.UPDATE)
            }
            binding.ivRemove.setOnClickListener {
                if (Constants.IsDemoMode) {

                    Utils.errorAlert(itemView.context as Activity,
                        itemView.resources.getString(R.string.demo_user_message))
                } else {
                    listener(pos,Constants.DELETE)

                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqViewHolder {
        val view = CellFaqBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FaqViewHolder(view)
    }

    override fun onBindViewHolder(holder: FaqViewHolder, position: Int) {
        holder.bindItems(faqList[position],position)
    }

    override fun getItemCount(): Int {
        return faqList.size
    }
}