package com.workdo.ticketgo.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellUsersBinding
import com.workdo.ticketgo.model.UsersDataItem
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.ExtensionFunctions.loadUrlCircle
import com.workdo.ticketgo.util.Utils

class UserAdapter(
    private val usersList: ArrayList<UsersDataItem>,
    val clickListener: (Int, String) -> Unit,
) : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
    inner class UserViewHolder(private val itemBinding: CellUsersBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: UsersDataItem, position: Int) = with(itemBinding)
        {
            itemBinding.apply {
                ivProfile.loadUrlCircle(data.avatarlink)
                tvUserName.text = data.name.toString()
                tvEmail.text = data.email.toString()


                linearDelete.setOnClickListener {

                    if (Constants.IsDemoMode) {

                        Utils.errorAlert(itemView.context as Activity,
                            itemView.resources.getString(R.string.demo_user_message))
                    } else {
                        clickListener(position, Constants.DELETE)

                    }

                }
                linearEdit.setOnClickListener {
                    if (Constants.IsDemoMode) {

                        Utils.errorAlert(itemView.context as Activity,
                            itemView.resources.getString(R.string.demo_user_message))
                    } else {
                        clickListener(position, Constants.UPDATE)
                    }
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = CellUsersBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bindItems(usersList[position], position)
    }

    override fun getItemCount(): Int {
        return usersList.size
    }
}