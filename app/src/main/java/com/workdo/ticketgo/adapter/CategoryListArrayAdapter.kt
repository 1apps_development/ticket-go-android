package com.workdo.ticketgo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.model.CategoryDataItem

class CategoryListArrayAdapter (context: Context, languageList:ArrayList<CategoryDataItem>):
    ArrayAdapter<CategoryDataItem>(context, R.layout.item_dropdown_menu,languageList){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return init(position,convertView,parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return init(position,convertView,parent)
    }
    private fun init (position: Int, convertView: View?, parent: ViewGroup): View {
        val data =getItem(position)
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.item_dropdown_menu, parent, false)
        val tvLanguageName = view.findViewById<TextView>(R.id.tvLanguageName)
        tvLanguageName.text=data?.name.toString()
        return view

    }




}