package com.workdo.ticketgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.databinding.CellCustomFieldBinding
import com.workdo.ticketgo.model.CustomFieldsItem

class CustomFieldAdapter(private val customFieldList:ArrayList<CustomFieldsItem>):RecyclerView.Adapter<CustomFieldAdapter.CustomViewHolder>()  {
    inner class CustomViewHolder(private val itemBinding:CellCustomFieldBinding):RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bindItems(data: CustomFieldsItem)= with(itemBinding)
        {
            edCustomFieldTitle.text = data.name.toString()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view=CellCustomFieldBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bindItems(customFieldList[position])
    }

    override fun getItemCount(): Int {
        return customFieldList.size
    }

}