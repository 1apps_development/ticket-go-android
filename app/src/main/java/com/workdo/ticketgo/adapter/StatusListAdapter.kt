package com.workdo.ticketgo.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.databinding.CellTicketListBinding
import com.workdo.ticketgo.model.StatusItem
import com.workdo.ticketgo.model.TicketDataItem
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import com.workdo.ticketgo.util.ExtensionFunctions.show
import java.util.*
import kotlin.collections.ArrayList

class StatusListAdapter(
    private val statusList: ArrayList<StatusItem>,
    private val listener: (TicketDataItem, String, Int,Int) -> Unit,

    ) : RecyclerView.Adapter<StatusListAdapter.StatusViewHolder>(){

    inner class StatusViewHolder(private val itemBinding: CellTicketListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: StatusItem, position: Int) = with(itemBinding)
        {
            itemBinding.tvTitle.text = data.status.toString().replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
            itemBinding.rvTickets.apply {
                layoutManager = LinearLayoutManager(itemView.context)
                adapter = data.tickets?.let {
                    TicketsAdapter(it) { data, type ,pos->

                        when (type) {
                            Constants.ItemClick -> {
                                listener(data, Constants.ItemClick,pos,position)

                            }
                            Constants.DELETE -> {
                                listener(data,Constants.DELETE,pos,position)
                            }
                            Constants.REPLY -> {
                                listener(data,Constants.REPLY,pos,position)

                            }
                        }
                    }

                }

            }

            if (data.expandable) itemBinding.rvTickets.show() else itemBinding.rvTickets.hide()

            itemView.setOnClickListener {
                isAnyItemExpanded(position)
                data.expandable = !data.expandable
                notifyItemChanged(position, Unit)
            }
        }
    }

    private fun isAnyItemExpanded(position: Int) {
        val temp = statusList.indexOfFirst {
            it.expandable
        }
        if (temp >= 0 && temp != position) {
            statusList[temp].expandable = false
            notifyItemChanged(temp, 0)
        }
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatusViewHolder {
        val view = CellTicketListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StatusViewHolder(view)
    }

    override fun onBindViewHolder(holder: StatusViewHolder, position: Int) {
        holder.bindItems(statusList[position], position)
    }


    override fun getItemCount(): Int {
        return statusList.size
    }




}