package com.workdo.ticketgo.adapter

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellCategoryBinding
import com.workdo.ticketgo.model.CategoryDataItem
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.Utils

class CategoriesAdapter(private val categoriesList: ArrayList<CategoryDataItem>,var listener:(Int,String)->Unit) :
    RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>() {

    inner class CategoriesViewHolder(private val itemBinding: CellCategoryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: CategoryDataItem,pos:Int) = with(itemBinding)
        {
            itemBinding.apply {
                tvCategoryName.text = data.name.toString()
                tvCategoryType.text = data.name.toString()

                val drawable = tvCategoryType.background as GradientDrawable
                drawable.setStroke(3, Color.parseColor(data.color))
                tvCategoryType.setTextColor(Color.parseColor(data.color))
                tvCategoryType.setBackgroundDrawable(drawable)
                linearDelete.setOnClickListener {
                    if(Constants.IsDemoMode)
                    {

                        Utils.errorAlert(itemView.context as Activity,itemView.resources.getString(R.string.demo_user_message))
                    } else
                    {
                        listener(pos,Constants.DELETE)

                    }

                }


                linearUpdate.setOnClickListener {
                    listener(pos,Constants.UPDATE)
                }
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view = CellCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        holder.bindItems(categoriesList[position],position)
    }

    override fun getItemCount(): Int {
        return categoriesList.size
    }
}