package com.workdo.ticketgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.databinding.CellHorizontalTicketsBinding
import com.workdo.ticketgo.model.TicketDataItem
import com.workdo.ticketgo.util.Constants

class NewTicketsAdapter(
    private val ticketList: ArrayList<TicketDataItem>,
    private val listener: (TicketDataItem, String) -> Unit,
) : RecyclerView.Adapter<NewTicketsAdapter.NewTicketViewHolder>() {

    inner class NewTicketViewHolder(private val itemBinding: CellHorizontalTicketsBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(dataItem: TicketDataItem) = with(itemBinding)
        {
            tvTicketId.text = "# ${dataItem.ticketId}"
            tvName.text = dataItem.name.toString()
            tvEmail.text = dataItem.email.toString()
            itemView.setOnClickListener {
                listener(dataItem,Constants.ItemClick)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewTicketViewHolder {
        val view =
            CellHorizontalTicketsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewTicketViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewTicketViewHolder, position: Int) {
        holder.bindItems(ticketList[position])
    }

    override fun getItemCount(): Int {
        return ticketList.size
    }
}