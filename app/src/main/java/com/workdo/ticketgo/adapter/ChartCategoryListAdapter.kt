package com.workdo.ticketgo.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellCategoryLegendBinding
import com.workdo.ticketgo.model.CategoryAnalyticsItem
import com.workdo.ticketgo.model.CategoryDataItem

class ChartCategoryListAdapter(private val categoriesList: ArrayList<CategoryAnalyticsItem>): RecyclerView.Adapter<ChartCategoryListAdapter.CategoriesViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): CategoriesViewHolder {

        val view=CellCategoryLegendBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return CategoriesViewHolder(view)
    }

    inner class CategoriesViewHolder(private val itemBinding: CellCategoryLegendBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: CategoryAnalyticsItem,pos:Int) = with(itemBinding)
        {
           val drawable = ContextCompat.getDrawable(itemView.context, R.drawable.bg_circle)!!

            val color = Color.parseColor(data.color)
            val colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
            drawable.colorFilter=colorFilter
            categoryView.background=drawable



            categoryName.text=data.category.toString()
            categoryName.setTextColor(Color.parseColor(data.color))
            val percentage= (data.value?.times(100) ?: 0) / 360
            tvPercentage.text=percentage.toString().plus("%")
        }
    }


    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        holder.bindItems(categoriesList[position],position)
    }

    override fun getItemCount(): Int {
        return categoriesList.size
    }
}