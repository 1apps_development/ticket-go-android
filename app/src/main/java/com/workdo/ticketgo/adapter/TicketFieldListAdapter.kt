package com.workdo.ticketgo.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellTicketFieldBinding
import com.workdo.ticketgo.model.CustomFieldItem
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.Utils

class TicketFieldListAdapter(
    private val ticketFieldList: ArrayList<CustomFieldItem>,
    var listener: (Int, String) -> Unit,
) : RecyclerView.Adapter<TicketFieldListAdapter.TicketFieldViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): TicketFieldViewHolder {

        val view =
            CellTicketFieldBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TicketFieldViewHolder(view)
    }


    override fun getItemCount(): Int {
        return ticketFieldList.size
    }

    inner class TicketFieldViewHolder(private val itemBinding: CellTicketFieldBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {


        fun bindItems(data: CustomFieldItem, pos: Int) = with(itemBinding)
        {



            itemBinding.apply {
                ivRemove.setOnClickListener {
                    if (Constants.IsDemoMode) {

                        Utils.errorAlert(itemView.context as Activity,
                            itemView.resources.getString(R.string.demo_user_message))
                    } else {

                        listener(pos, Constants.DELETE)
                    }
                }

                tvLabelName.text = data.name.toString()
                tvPlaceHolder.text = data.placeholder.toString()
            }
        }


    }


    override fun onBindViewHolder(holder: TicketFieldViewHolder, position: Int) {

        holder.bindItems(ticketFieldList[position], position)
    }
}