package com.workdo.ticketgo.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.CellCategoryLegendBinding
import com.workdo.ticketgo.model.AnalyticsData

class TicketAnalyticsAdapter(private val analyticsList:ArrayList<AnalyticsData>):RecyclerView.Adapter<TicketAnalyticsAdapter.AnalyticsViewHolder> (){



    inner class AnalyticsViewHolder(private val itemBinding:CellCategoryLegendBinding):RecyclerView.ViewHolder(itemBinding.root)
    {
         fun bindItems(data:AnalyticsData)= with(itemBinding)
        {
            tvPercentage.text=data.percentage.plus("%")
            categoryName.text=data.name

            val drawable = ContextCompat.getDrawable(itemView.context, R.drawable.bg_circle)!!

            // Apply color filter to the drawable
            val color = Color.parseColor(data.color)
            val colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)

            drawable.colorFilter=colorFilter
            categoryView.background=drawable


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnalyticsViewHolder {
        val view=CellCategoryLegendBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return AnalyticsViewHolder(view)
    }

    override fun onBindViewHolder(holder: AnalyticsViewHolder, position: Int) {
        holder.bindItems(analyticsList[position])
    }

    override fun getItemCount(): Int {
        return  analyticsList.size
    }
}