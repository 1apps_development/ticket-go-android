package com.workdo.ticketgo.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.ticketgo.databinding.CellAttachmentBinding
import com.workdo.ticketgo.util.Utils

class ImageListAdapter(private val imageList:ArrayList<Uri>):RecyclerView.Adapter<ImageListAdapter.ImageListViewHolder>() {
    inner class ImageListViewHolder(private val itemBinding:CellAttachmentBinding):RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bindItems(data:Uri,pos:Int)= with(itemBinding)
        {
            itemBinding.ivRemove.setOnClickListener {
                imageList.removeAt(pos)
                notifyDataSetChanged()
            }
            itemBinding.tvImageFileName.text=
                Utils.getFileNameFromUri(itemView.context.contentResolver,data).toString()

            itemBinding.tvFileSize.text=
                Utils.getFileSizeFromUri(itemView.context.contentResolver,data).plus("MB")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder {
        val view=CellAttachmentBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ImageListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
            holder.bindItems(imageList[position],position)
    }

    override fun getItemCount(): Int {
        return imageList.size
    }
}