package com.workdo.ticketgo.app

import android.app.Application
import androidx.multidex.MultiDex

class MyApp :Application() {

    companion object {
        lateinit var app: MyApp



        fun getInstance(): MyApp {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()

        app = this

        MultiDex.install(this)

    }
}