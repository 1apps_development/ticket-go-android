package com.workdo.ticketgo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.ticketgo.adapter.MenuAdapter
import com.workdo.ticketgo.databinding.ActivityMainBinding
import com.workdo.ticketgo.databinding.DialogMenuBinding
import com.workdo.ticketgo.fragment.*
import com.workdo.ticketgo.model.MenuItemData
import com.github.techisfun.android.topsheet.TopSheetDialog
import com.workdo.ticketgo.activity.ActFaq
import com.workdo.ticketgo.util.*
import com.workdo.ticketgo.util.ExtensionFunctions.loadUrlCircle
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(),BottomSheetCallBack {
    private lateinit var binding:ActivityMainBinding
    private var itemList = ArrayList<MenuItemData>()
    private lateinit var menuAdapter: MenuAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val time = Calendar.getInstance().time
        val formatter = SimpleDateFormat("EEEE,dd MMM",Locale.getDefault())
        val todayDate = formatter.format(time)
        binding.tvDate.text=todayDate

        binding.bottomNavigation.selectedItemId = R.id.ivDashBoard
        binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)

        menuItemData()
        initClickListeners()
        Common.replaceFragment(
            supportFragmentManager,
            FragDashBoard(),
            R.id.fragContainer)

        bottomBarItemNavigation()
    }


    private fun initClickListeners()
    {
        binding.ivDrawer.setOnClickListener {
            openTopSheetDialog()
        }
    }


    private fun menuSelection(pos: Int) {
        for (j in 0 until itemList.size) {
            itemList[j].isSelect = false

        }
        itemList[pos].isSelect = true
    }


    private  fun menuItemData()
    {
        itemList.add(MenuItemData(resources.getString(R.string.dashboard),ResourcesCompat.getDrawable(resources,R.drawable.ic_dashboard,null)!!,true))

        itemList.add(MenuItemData(resources.getString(R.string.users), ResourcesCompat.getDrawable(resources,R.drawable.ic_user,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.tickets), ResourcesCompat.getDrawable(resources,R.drawable.ic_ticket,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.categories), ResourcesCompat.getDrawable(resources,R.drawable.ic_folder,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.faq), ResourcesCompat.getDrawable(resources,R.drawable.ic_faq,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.settings), ResourcesCompat.getDrawable(resources,R.drawable.ic_settings,null)!!,false))

    }


    private fun bottomBarItemNavigation() {
        binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
            when (item.itemId) {
                R.id.ivTicket -> {

                    menuSelection(2)

                    binding.tvAppBarTitle.text = resources.getString(R.string.tickets)
                    if (fragment !is FragTickets) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTickets(),
                            R.id.fragContainer
                        )
                    }

                    true


                }
                R.id.ivCategories -> {
                    menuSelection(3)

                    binding.tvAppBarTitle.text = resources.getString(R.string.categories)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragCategories(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivDashBoard -> {
                    menuSelection(0)

                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragDashBoard(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivUsers -> {
                    menuSelection(1)

                    binding.tvAppBarTitle.text = resources.getString(R.string.users)

                    Common.replaceFragment(supportFragmentManager, FragUsers(), R.id.fragContainer)

                    true
                }

                R.id.ivSetting -> {
                    menuSelection(5)

                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragSettings(),
                        R.id.fragContainer
                    )

                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun openTopSheetDialog() {
        val dialog = TopSheetDialog(this)
        val bottomSheetBinding = DialogMenuBinding.inflate(layoutInflater)

        bottomSheetBinding.ivProfile.loadUrlCircle(SharePreference.getStringPref(this@MainActivity,SharePreference.userProfile))
        bottomSheetBinding.tvUserName.text=SharePreference.getStringPref(this@MainActivity,SharePreference.userName)
        bottomSheetBinding.tvEmail.text=SharePreference.getStringPref(this@MainActivity,SharePreference.userEmail)
        bottomSheetBinding.btnEdit.setOnClickListener {


                val fragEditProfile = FragEditProfile()
                val bundle = Bundle()
                bundle.putString("type", "UPDATE")
                bundle.putString("user_id",
                    SharePreference.getStringPref(this@MainActivity, SharePreference.userId))
                fragEditProfile.arguments = bundle


                fragEditProfile.setCallback(this)
                fragEditProfile.show(supportFragmentManager, FragEditProfile::class.java.name)

        }
        for( i in 0 until itemList.size)
        {
            if(itemList[i].isSelect)
            {
                bottomSheetBinding.tvTitle.text=itemList[i].itemName
            }
        }

        menuAdapter = MenuAdapter(this@MainActivity, itemList) { s: String, i: Int ->
            if (s == "itemClick") {
                val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
                dialog.dismiss()

                if (i == 0) {
                    binding.bottomNavigation.selectedItemId=R.id.ivDashBoard
                    bottomSheetBinding.tvTitle.text=resources.getString(R.string.dashboard)

                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)
                    if (fragment !is FragDashBoard) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragDashBoard(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 1) {
                    binding.bottomNavigation.selectedItemId=R.id.ivUsers
                    bottomSheetBinding.tvTitle.text=resources.getString(R.string.users)

                    binding.tvAppBarTitle.text = resources.getString(R.string.users)
                    if (fragment !is FragUsers) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragUsers(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 2) {
                    binding.bottomNavigation.selectedItemId=R.id.ivTicket
                    bottomSheetBinding.tvTitle.text=resources.getString(R.string.tickets)

                    binding.tvAppBarTitle.text = resources.getString(R.string.tickets)
                    if (fragment !is FragTickets) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTickets(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 3) {
                    binding.bottomNavigation.selectedItemId=R.id.ivCategories
                    bottomSheetBinding.tvTitle.text=resources.getString(R.string.categories)

                    binding.tvAppBarTitle.text = resources.getString(R.string.categories)
                    if (fragment !is FragCategories) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragCategories(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 4) {
//                    bottomSheetBinding.tvTitle.text=resources.getString(R.string.faq)
//
//                    binding.tvAppBarTitle.text = resources.getString(R.string.faq)

                   startActivity(Intent(this@MainActivity,ActFaq::class.java))

                   /* if (fragment !is FragFaq) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager, FragFaq(), R.id.fragContainer)
                    }*/
                }  else if (i == 5) {
                    binding.bottomNavigation.selectedItemId=R.id.ivSetting
                    bottomSheetBinding.tvTitle.text=resources.getString(R.string.settings)

                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)
                    if (fragment !is FragSettings) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSettings(),
                            R.id.fragContainer
                        )
                    }
                }

                for (j in 0 until itemList.size) {
                    itemList[j].isSelect = false

                }

                bottomSheetBinding.tvTitle.text=itemList[i].itemName.toString()
                itemList[i].isSelect = true
                menuAdapter.notifyDataSetChanged()
            }

        }
        bottomSheetBinding.ivClose.setOnClickListener {
            dialog.dismiss()
        }
        bottomSheetBinding.rvMenu.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 4)
            adapter = menuAdapter
            isNestedScrollingEnabled = true
        }
        dialog.setContentView(bottomSheetBinding.root)
        dialog.show()
    }

    override fun resultSuccess(result: String) {

    }




}