package com.workdo.ticketgo.fragment

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.NewTicketsAdapter
import com.workdo.ticketgo.adapter.StatusListAdapter
import com.workdo.ticketgo.adapter.TicketAnalyticsAdapter
import com.workdo.ticketgo.adapter.TicketsAdapter
import com.workdo.ticketgo.databinding.FragTicketsBinding
import com.workdo.ticketgo.model.*
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.*
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import com.workdo.ticketgo.util.ExtensionFunctions.show
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class FragTickets : Fragment(), BottomSheetCallBack {
    private var _binding: FragTicketsBinding? = null
    private val binding get() = _binding!!
    private val ticketList = ArrayList<StatusItem>()
    private lateinit var newTicketsAdapter: NewTicketsAdapter
    private val ticketDataList = ArrayList<TicketDataItem>()
    private lateinit var statusListAdapter: StatusListAdapter
    private var deleteCallBack: DeleteTicketCallBack? = null
    private val categoryList = ArrayList<CategoryDataItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragTicketsBinding.inflate(layoutInflater)
        return binding.root
    }


    fun setCallback(callback: DeleteTicketCallBack) {
        this.deleteCallBack = callback
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners()


        binding.pieChart.description.isEnabled = false
        binding.pieChart.setUsePercentValues(false)
        binding.pieChart.setEntryLabelTextSize(12f)
        binding.pieChart.centerText = ""
        binding.pieChart.isDrawHoleEnabled = true
        binding.pieChart.setCenterTextSize(22f)
        binding.pieChart.legend.isEnabled = false

        lifecycleScope.launch {
            val ticketList=async { callGetTicketList() }

            ticketList.await()
        }


        callGetTicketList()

        newTicketsAdapter = NewTicketsAdapter(ticketDataList) { data, type ->
            if (type == Constants.ItemClick) {
                val fragAddNewTicket = FragAddNewTicket()
                val bundle = Bundle()
                bundle.putString("type", Constants.UPDATE)
                bundle.putParcelable("ticket", data)
                fragAddNewTicket.arguments = bundle
                fragAddNewTicket.setCallback(this)
                fragAddNewTicket.show(requireActivity().supportFragmentManager,
                    FragAddNewTicket::class.java.name)
            }

        }


        statusListAdapter = StatusListAdapter(ticketList) { data, type,pos ,position->
            when (type) {
                Constants.ItemClick -> {
                    val fragAddNewTicket = FragAddNewTicket()
                    val bundle = Bundle()
                    bundle.putString("type", Constants.UPDATE)
                    bundle.putParcelable("ticket", data)
                    fragAddNewTicket.setCallback(this)
                    fragAddNewTicket.arguments = bundle
                    fragAddNewTicket.show(requireActivity().supportFragmentManager,
                        FragAddNewTicket::class.java.name)
                }
                Constants.REPLY -> {
                    val fragReplayTicket = FragReplayTicket()
                    val bundle=Bundle()
                    bundle.putString("id",data.id.toString())
                    fragReplayTicket.arguments=bundle
                    fragReplayTicket.show(requireActivity().supportFragmentManager, FragReplayTicket::class.java.name)
                }
                Constants.DELETE -> {
                    deleteTicket(data.id.toString(),pos,position)
                }
            }

        }
    }


    private fun setDataIntoChart(analyticsData: Analytics) {
        val entries: MutableList<PieEntry> = ArrayList()
        entries.add(PieEntry(analyticsData.newTicket?.toFloat()?:1f, "New Ticket"))
        entries.add(PieEntry(analyticsData.openTicket?.toFloat()?:1f, "Open Ticket"))
        entries.add(PieEntry(analyticsData.closeTicket?.toFloat()?:1f, "Close Ticket"))
        val analyticsList = ArrayList<AnalyticsData>()

        analyticsList.add(AnalyticsData(1,
            "#162C4E",
            analyticsData.newTicket.toString(),
            "New Ticket"))
        analyticsList.add(AnalyticsData(2,
            "#6FD943",
            analyticsData.openTicket.toString(),
            "Open Ticket"))
        analyticsList.add(AnalyticsData(3,
            "#FC275A",
            analyticsData.closeTicket.toString(),
            "Close Ticket"))

        binding.rvLegend.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = TicketAnalyticsAdapter(analyticsList)
        }

        val dataSet = PieDataSet(entries, "Tickets")

        val colors = listOf(Color.parseColor("#162C4E"),
            Color.parseColor("#6FD943"),
            Color.parseColor("#FC275A"))

        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f
        dataSet.colors = colors.toMutableList()
        val data = PieData(dataSet)

        binding.pieChart.data = data
        binding.pieChart.data.dataSet.setDrawValues(false)
        binding.pieChart.invalidate()
    }

    private fun setupTicketAdapter() {
        binding.rvTickets.layoutManager = LinearLayoutManager(requireActivity())
        binding.rvTickets.adapter = statusListAdapter

    }

    private fun callGetTicketList() {
        val request = HashMap<String, String>()
        request["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""
        request["search"] = ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).ticket(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            ticketList.clear()
                            response.body.data?.status?.let { ticketList.addAll(it) }
                            setupTicketAdapter()
                            response.body.data?.analytics?.let { setDataIntoChart(it) }

                            if ((response.body.data?.ticket?.data?.size ?: 0) == 0) {
                                binding.clTicketList.hide()
                            }
                            ticketDataList.clear()
                            response.body.data?.ticket?.data?.let { ticketDataList.addAll(it) }
                            if(ticketDataList.size>0)
                            {
                                binding.rvTickets.show()
                                binding.clTicketList.show()
                                binding.cvChart.show()
                                binding.tvNoDataFound.hide()
                            }else
                            {
                                binding.rvTickets.hide()
                                binding.clTicketList.hide()
                                binding.cvChart.hide()
                                binding.tvNoDataFound.show()
                            }
                            binding.rvNewTickets.apply {
                                layoutManager = LinearLayoutManager(requireActivity())
                                adapter = newTicketsAdapter
                            }
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun deleteTicket(id: String,childpos:Int,parentPos:Int) {
        val request = HashMap<String, String>()
        request["id"] = id
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity()).deleteTicket(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            ticketList.get(parentPos).tickets?.removeAt(childpos)
                            statusListAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }




    private fun initClickListeners() {
        binding.btnAddNewTicket.setOnClickListener {

            val fragAddNewTicket = FragAddNewTicket()
            fragAddNewTicket.setCallback(this)
            fragAddNewTicket.show(requireActivity().supportFragmentManager,
                FragAddNewTicket::class.java.name)
        }
    }



    override fun resultSuccess(result: String) {
        ticketList.clear()
        ticketDataList.clear()
        callGetTicketList()

    }
}