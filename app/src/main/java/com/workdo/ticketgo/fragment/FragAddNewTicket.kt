package com.workdo.ticketgo.fragment

import android.app.Activity
import android.app.Dialog
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.CategoryListArrayAdapter
import com.workdo.ticketgo.adapter.CustomFieldAdapter
import com.workdo.ticketgo.adapter.ImageListAdapter
import com.workdo.ticketgo.databinding.FragAddNewTicketBinding
import com.workdo.ticketgo.model.CategoryDataItem
import com.workdo.ticketgo.model.CustomFieldsItem
import com.workdo.ticketgo.model.TicketDataItem
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.BottomSheetCallBack
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import kotlin.collections.indices as indices


class FragAddNewTicket : BottomSheetDialogFragment() {
    private val mBehavior: BottomSheetBehavior<View>? = null
    private var callback: BottomSheetCallBack? = null

    private  var _binding: FragAddNewTicketBinding?=null
    private lateinit var imageAdapter: ImageListAdapter
    private lateinit var customFieldAdapter: CustomFieldAdapter
    private var categoryId = ""
    private var status = ""
    private val statusList = listOf("In Progress", "On Hold", "Closed")

    private val filesList = ArrayList<File>()
    private val imageList = ArrayList<Uri>()
    private val customFieldList = ArrayList<CustomFieldsItem>()
    private val categoryList = ArrayList<CategoryDataItem>()
    private lateinit var filePickerLauncher: ActivityResultLauncher<Array<String>>
    private var ticketData=TicketDataItem()

    private var id=""
    private  val binding get() = _binding!!
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)

        mBehavior = BottomSheetBehavior.from(view?.parent as View)


    }

    fun setCallback(callback: BottomSheetCallBack) {
        this.callback = callback
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragAddNewTicketBinding.inflate(layoutInflater)
        return binding.root
    }

    private fun initClickListeners() {
        binding.ivClose.setOnClickListener {
            dialog?.dismiss()
        }

        binding.constraintChooseFav.setOnClickListener {
            selectFiles()
        }

        lifecycleScope.launch {
            val customField = async { geCustomField() }
            val getCategory = async { getCategory() }
            customField.await()
            getCategory.await()
        }



        binding.tvCreateNewTicket.setOnClickListener {
            if (binding.edName.text.toString().isEmpty()) {
                Utils.errorAlert(requireActivity(), "Name Field Should Not Be Empty")
            } else if (binding.edEmail.toString().isEmpty()) {
                Utils.errorAlert(requireActivity(), "Email Field Should Not Be Empty")

            } else if (categoryId.isEmpty()) {
                Utils.errorAlert(requireActivity(), "Category Field Should Not Be Empty")

            } else if (status.isEmpty()) {
                Utils.errorAlert(requireActivity(), "Status Field Should Not Be Empty")

            } else if (binding.edSubject.text?.isEmpty() == true) {
                Utils.errorAlert(requireActivity(), "Subject Field Should Not Be Empty")

            } else if (binding.edSubjectDescription.text?.isEmpty() == true) {
                Utils.errorAlert(requireActivity(), "Description Field Should Not Be Empty")

            } else if (imageList.size == 0) {
                Utils.errorAlert(requireActivity(), "Image Attachment Is Required")

            } else {

                createTicket(id)



            }


        }


        setStatusAdapter()

    }


    private fun geCustomField() {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).getCustomField(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.customFields?.let { customFieldList.addAll(it) }
                            binding.rvCustomField.apply {
                                layoutManager = LinearLayoutManager(requireActivity())
                                adapter = CustomFieldAdapter(customFieldList)
                            }

                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun getCategory() {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity()).ticketCategory("1", request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    when (response.body.status) {
                        1 -> {
                            response.body.data?.category?.let { categoryList.addAll(it) }
                            setCategoryAdapter(categoryList)


                            for (i in 0 until categoryList.size) {
                                if (ticketData.category.toString() == categoryList[i].name) {
                                    binding.edCategory.setSelection(i)
                                    categoryId=categoryList[i].id.toString()
                                }
                            }

                            for (i in statusList.indices) {
                                if (ticketData.status == statusList[i]) {
                                    binding.edStatus.setSelection(i)
                                    status=statusList[i]
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun setStatusAdapter() {

        val statusAdapter = ArrayAdapter(requireActivity(), R.layout.item_dropdown_menu, statusList)
        binding.edStatus.adapter = statusAdapter
        binding.edStatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                binding.edStatus.setSelection(p2)
                status = statusList[p2]

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }


    }


    private fun setCategoryAdapter(categoryList: ArrayList<CategoryDataItem>) {


        val categoryAdapter = CategoryListArrayAdapter(requireActivity(), categoryList)
        binding.edCategory.adapter = categoryAdapter
        binding.edCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                binding.edCategory.setSelection(p2)
                categoryId = categoryList[p2].id.toString()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
    }

    private fun createTicket(id:String) {

        val id = Utils.setRequestBody(id)
        val name = Utils.setRequestBody(binding.edName.text.toString())
        val email = Utils.setRequestBody(binding.edEmail.text.toString())
        val subject = Utils.setRequestBody(binding.edSubject.text.toString())
        val description = Utils.setRequestBody(binding.edSubjectDescription.text.toString())
        val category = Utils.setRequestBody(categoryId)
        val status = Utils.setRequestBody(status)


        val imageParts = ArrayList<MultipartBody.Part>()
        for (uri in 0 until imageList.size) {

            val file = File(imageList[uri].path.toString())

            val requestFile = file.asRequestBody("image/*".toMediaType())
            val part = MultipartBody.Part.createFormData("attachments[]", file.name, requestFile)
            imageParts.add(part)
        }

        val hashmap = HashMap<String, RequestBody>()

        for (i in 0 until customFieldList.size) {
            hashmap["custom_field[${customFieldList[i].id}]"] =
                Utils.setRequestBody(customFieldList[i].name.toString())
        }



        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).ticketCreate(id,
                name,
                email,
                category,
                subject,
                status,
                description,
                imageParts,
                hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            callback?.resultSuccess("success")
                            dialog?.dismiss()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else
                    {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())

                    }


                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun setupImageListAdapter() {
        binding.rvImageList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = imageAdapter
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val type = arguments?.getString("type") ?: ""

        initClickListeners()
        if (type == Constants.UPDATE) {

            binding.tvNewTicketTitle.text = resources.getString(R.string.update_ticket)
            binding.tvCreateNewTicket.text=resources.getString(R.string.update_)
            binding.tvTitle.text = resources.getString(R.string.update)
            ticketData = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                arguments?.getParcelable("ticket", TicketDataItem::class.java)
                    ?.let { setupData(it) }
                arguments?.getParcelable("ticket", TicketDataItem::class.java)!!


            } else {
                val data = arguments?.getParcelable<TicketDataItem>("ticket")
                data?.let { setupData(it) }
                data!!

            }
        }




        imageAdapter = ImageListAdapter(imageList)
        setupImageListAdapter()
        filePickerLauncher =
            registerForActivityResult(ActivityResultContracts.OpenMultipleDocuments()) { uris ->
                uris?.let {
                    for (uri in uris) {
                        imageList.add(uri)
                    }
                    imageAdapter.notifyDataSetChanged()

                }

            }


    }


    private fun setupData(data: TicketDataItem) {
        id=data.id.toString()
        binding.edName.setText(data.name.toString())
        binding.edEmail.setText(data.email.toString())

        binding.edSubject.setText(data.subject.toString())
        binding.edSubjectDescription.setText(data.description.toString())
    }

    private val pickMedia = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
            if (activityResult.resultCode == Activity.RESULT_OK) {
                activityResult.data?.data?.let { imageList.add(it) }
                imageAdapter.notifyDataSetChanged()
            }
        }


    private fun selectFiles() {
        ImagePicker.with(requireActivity()).cropSquare().compress(1024)
            .saveDir(File(requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                resources.getString(R.string.app_name))).maxResultSize(512, 512).createIntent {
                pickMedia.launch(it)
            }
    }

    override fun onStart() {
        super.onStart()
        mBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Utils.dismissLoadingProgress()

    }
}