package com.workdo.ticketgo.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.linroid.filtermenu.library.FilterMenu
import com.linroid.filtermenu.library.FilterMenuLayout
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.NewTicketsAdapter
import com.workdo.ticketgo.adapter.TicketAnalyticsAdapter
import com.workdo.ticketgo.adapter.TicketsAdapter
import com.workdo.ticketgo.databinding.FragDashBoardBinding
import com.workdo.ticketgo.databinding.NewCategoryDialogBinding
import com.workdo.ticketgo.model.*
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.*
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class FragDashBoard : Fragment(),BottomSheetCallBack {
    private var _binding: FragDashBoardBinding? = null
    private val ticketList = ArrayList<TicketDataItem>()
    private val xAxisList = ArrayList<String>()
    private val binding get() = _binding!!
    private lateinit var deleteTicketCallBack: DeleteTicketCallBack
    private lateinit var ticketsAdapter: TicketsAdapter

    private val colorPickerDialog: ColorPickerDialog by lazy {
        ColorPickerDialog(requireActivity())
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragDashBoardBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        attachMenu(binding.filterMenu)
        getHomePage()


        binding.tvBaseUrl.text= SharePreference.getStringPref(requireActivity(),SharePreference.BaseUrl)

        binding.pieChart.description.isEnabled = false
        binding.pieChart.setUsePercentValues(false)
        binding.pieChart.setEntryLabelTextSize(12f)
        binding.pieChart.centerText = ""
        binding.pieChart.isDrawHoleEnabled = true
        binding.pieChart.setCenterTextSize(22f)
        binding.pieChart.legend.isEnabled = false
        ticketsAdapter=TicketsAdapter(ticketList){ticketData,type ,pos->
            when (type) {
                Constants.ItemClick -> {

                    val fragAddNewTicket=FragAddNewTicket()
                    val bundle= Bundle()
                    bundle.putString("type",Constants.UPDATE)
                    bundle.putParcelable("ticket",ticketData)

                    fragAddNewTicket.arguments=bundle
                    fragAddNewTicket.show(requireActivity().supportFragmentManager,FragAddNewTicket::class.java.name)

                }
                Constants.DELETE -> {
                    deleteTicket(ticketData.id.toString(),pos)
                }
                Constants.REPLY -> {
                    val fragReplayTicket = FragReplayTicket()
                    val bundle=Bundle()
                    bundle.putString("id",ticketData.id.toString())
                    fragReplayTicket.arguments=bundle
                    fragReplayTicket.show(requireActivity().supportFragmentManager,
                        FragReplayTicket::class.java.name)
                }
            }


        }


        binding.btnEdit.setOnClickListener {


                    val fragEditProfile = FragEditProfile()
                    val bundle=Bundle()
                    bundle.putString("type","UPDATE")
                    bundle.putString("user_id",SharePreference.getStringPref(requireActivity(),SharePreference.userId))
                    fragEditProfile.arguments=bundle


                    fragEditProfile.setCallback(this)
                    fragEditProfile.show(requireActivity().supportFragmentManager, FragEditProfile::class.java.name)


        }

    }

    private fun setupTicketAdapter() {
        binding.rvTicketsList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = ticketsAdapter
        }
    }


    private fun deleteTicket(id: String,childpos:Int) {
        val request = HashMap<String, String>()
        request["id"] = id
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity()).deleteTicket(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            ticketList.removeAt(childpos)
                            ticketsAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())

                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun getHomePage() {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).home(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.lastTicket?.let { ticketList.addAll(it) }
                            response.body.data?.graphData?.xAxis?.let { xAxisList.addAll(it) }
                            setupData(response.body.data?.statistics!!,response.body.data.userData!!)

                            if(ticketList.size==0)
                            {
                                binding.rvTicketsList.hide()
                                binding.tvLastTicketTitle.hide()
                            }

                            setupTicketAdapter()

                            setUpLineChart()
                            response.body.data.graphData?.yAxis?.let { setDataToLineChart(it) }
                            response.body.data.ticketAnalytics?.let { setupTicketPieChart(it) }

                                if((response.body.data.categoryAnalytics?.size ?: 0) > 0)
                                {
                                    response.body.data.categoryAnalytics?.let { setCategoryData(it) }

                                }else
                                {
                                    val categoriesList= ArrayList<CategoryAnalyticsItem>()
                                    categoriesList.add(CategoryAnalyticsItem("#FC275A","Bug",190))
                                    categoriesList.add(CategoryAnalyticsItem("#153364","Resolved",120))
                                    categoriesList.add(CategoryAnalyticsItem("#6FD943","Errors",90))
                                    setCategoryData(categoriesList)
                                }

                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    } else {
                        Utils.errorAlert(requireActivity(),
                            response.body.data?.message ?: response.body.message.toString())
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun setupData(data: Statistics, userData: UserData) {
        binding.tvAgentsCount.text = data.agents.toString()
        binding.tvCategoriesCount.text = data.category.toString()
        binding.tvTicketCount.text = data.openTicket.toString()
        binding.tvClosedTickets.text = data.closeTicket.toString()

        binding.tvUserName.text=userData.name.toString()
        binding.tvUserDescription.text=userData.email.toString()
        binding.tvTicketText.text="you have new ${userData.totalTicket} Ticket"

        binding.tvTotalTicket.text=userData.totalTicket.toString()


    }


    private fun setupTicketPieChart(analyticsData: TicketAnalytics) {
        binding.ticketPieChart.description.isEnabled = false
        binding.ticketPieChart.setUsePercentValues(false)
        binding.ticketPieChart.setEntryLabelTextSize(12f)
        binding.ticketPieChart.centerText = ""
        binding.ticketPieChart.isDrawHoleEnabled = true
        binding.ticketPieChart.setCenterTextSize(22f)
        binding.ticketPieChart.legend.isEnabled = false

        val entries: MutableList<PieEntry> = ArrayList()

        if(analyticsData.closeTicket==0 && analyticsData.newTicket==0 &&analyticsData.openTicket==0)
        {
            entries.add(PieEntry(20f ,"New Ticket"))
            entries.add(PieEntry(30f,"Open Ticket"))
            entries.add(PieEntry(40f, "Close Ticket"))
        }else
        {
            entries.add(PieEntry(analyticsData.newTicket?.toFloat()!!, "New Ticket"))
            entries.add(PieEntry(analyticsData.openTicket?.toFloat()!!,"Open Ticket"))
            entries.add(PieEntry(analyticsData.closeTicket?.toFloat()!!, "Close Ticket"))
        }


        val analyticsList = ArrayList<AnalyticsData>()

        analyticsList.add(AnalyticsData(1,
            "#162C4E",
            analyticsData.newTicket.toString(),
            "New Ticket"))
        analyticsList.add(AnalyticsData(2,
            "#6FD943",
            analyticsData.openTicket.toString(),
            "Open Ticket"))
        analyticsList.add(AnalyticsData(3,
            "#FC275A",
            analyticsData.closeTicket.toString(),
            "Close Ticket"))

        binding.rvLegend.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = TicketAnalyticsAdapter(analyticsList)
        }


        val dataSet = PieDataSet(entries, "Tickets")

        val colors= listOf(Color.parseColor(  "#162C4E"),Color.parseColor( "#6FD943"),Color.parseColor("#FC275A"))

        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f
        dataSet.colors = colors.toMutableList()
        val data = PieData(dataSet)

        binding.ticketPieChart.data = data
        binding.ticketPieChart.data.dataSet.setDrawValues(false)
        binding.ticketPieChart.invalidate()


    }




    private fun setDataToLineChart(yAxisValue: ArrayList<YAxisItem>) {


        val closeTicketList = ArrayList<Entry>()
        val openTicketList = ArrayList<Entry>()



        for (i in 0 until yAxisValue[0].data?.size!!) {
            openTicketList.add(Entry(i.toFloat(), yAxisValue[0].data?.get(i)?.toFloat()!!))
        }


        for (i in 0 until yAxisValue[1].data?.size!!) {
            closeTicketList.add(Entry(i.toFloat(), yAxisValue[1].data?.get(i)?.toFloat()!!))
        }


        val openTicketData = LineDataSet(openTicketList, yAxisValue[0].name)
        openTicketData.lineWidth = 1.5f
        openTicketData.setDrawValues(false)
        openTicketData.setDrawCircles(false)
        openTicketData.mode = LineDataSet.Mode.CUBIC_BEZIER
        openTicketData.color = Color.parseColor(yAxisValue[0].color)

        val closeTicketData = LineDataSet(closeTicketList, yAxisValue[1].name)
        closeTicketData.lineWidth = 1.5f
        closeTicketData.setDrawValues(false)
        closeTicketData.setDrawCircles(false)
        closeTicketData.mode = LineDataSet.Mode.CUBIC_BEZIER
        closeTicketData.color = Color.parseColor(yAxisValue[1].color)


        val dataSet = ArrayList<ILineDataSet>()
        dataSet.add(openTicketData)
        dataSet.add(closeTicketData)

        val lineData = LineData(dataSet)
        binding.lineChart.data = lineData

        binding.lineChart.invalidate()

    }


    private fun setUpLineChart() {
        with(binding.lineChart) {

            setExtraOffsets(10f, 10f, 10f, 10f)

            axisRight.isEnabled = false
            animateX(1, Easing.EaseInSine)

            description.isEnabled = false

            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.valueFormatter = MyAxisFormatter()
            xAxis.granularity = 1F
            xAxis.setDrawGridLines(true)
            xAxis.setDrawAxisLine(false)
            axisLeft.setDrawGridLines(false)
            extraRightOffset = 1f

            legend.isEnabled = true
            legend.orientation = Legend.LegendOrientation.HORIZONTAL
            legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
            legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
            legend.form = Legend.LegendForm.DEFAULT


        }


    }

    private fun setCategoryData(categoryAnalyticsList:ArrayList<CategoryAnalyticsItem>) {
        val entries: MutableList<PieEntry> = ArrayList()
        val colorsList=ArrayList<Int>()
        for (i in 0 until categoryAnalyticsList.size) {
            entries.add(PieEntry(categoryAnalyticsList[i].value?.toFloat()!!, categoryAnalyticsList[i].category))
            colorsList.add(Color.parseColor(categoryAnalyticsList[i].color))
        }

        val dataSet = PieDataSet(entries, "Categories")

        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f
        dataSet.colors = colorsList.toMutableList()
        val data = PieData(dataSet)

        binding.pieChart.data = data
        binding.pieChart.data.dataSet.setDrawValues(false)
        binding.pieChart.invalidate()
    }

    inner class MyAxisFormatter : IndexAxisValueFormatter() {


        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            return if (index < xAxisList.size && index >= 0) {
                xAxisList[index]
            } else {
                ""
            }

        }

    }


    private val listener: FilterMenu.OnMenuChangeListener = object :
        FilterMenu.OnMenuChangeListener {
        override fun onMenuItemClick(view: View, position: Int) {


            when (position) {
                0 -> {

                    val fragNewTicket = FragAddNewTicket()
                    fragNewTicket.show(requireActivity().supportFragmentManager, fragNewTicket::class.java.name)

                }
                1 -> {
                    newCategoryDialog()

                }
                2 -> {
                    val fragNewUser = FragNewUser()
                    fragNewUser.show(requireActivity().supportFragmentManager, FragNewUser::class.java.name)

                }
            }
        }


        private fun newCategoryDialog() {
            val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
            val bottomSheetBinding = NewCategoryDialogBinding.inflate(layoutInflater)
            bottomSheetDialog.setContentView(bottomSheetBinding.root)
            bottomSheetBinding.ivClose.setOnClickListener {
                bottomSheetDialog.dismiss()
            }
            val color = ResourcesCompat.getColor(resources, R.color.lime_green, null)
            val colorHex = String.format("#%06X", color)

            bottomSheetBinding.tvColorCode.text = colorHex
            bottomSheetBinding.tvColorCode.setOnClickListener {
                showColorPickerDialog(bottomSheetBinding.tvColorCode)
            }
            bottomSheetBinding.btnCreate.setOnClickListener {
                if (bottomSheetBinding.edCategoryName.text?.isEmpty() == true) {
                    Utils.errorAlert(requireActivity(), "Category Name is Required")
                } else {
                    addCategory("",
                        bottomSheetBinding.edCategoryName.text.toString(),
                        bottomSheetBinding.tvColorCode.text.toString(), bottomSheetDialog)
                }
            }


            bottomSheetDialog.show()
        }



        override fun onMenuCollapse() {

        }

        override fun onMenuExpand() {

        }
    }

    private fun showColorPickerDialog(textview: AppCompatTextView) {
        colorPickerDialog.showColorPickerDialog { color ->
            val hexColor = String.format("#%06X", 0xFFFFFF and color)
            textview.setBackgroundColor(color)
            textview.text = hexColor

        }
    }

    private fun addCategory(
        id: String,
        name: String,
        colorCode: String,
        dialog: BottomSheetDialog,
    ) {
        val request = HashMap<String, String>()
        request["id"] = id
        request["name"] = name
        request["color"] = colorCode

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).createCategory(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            dialog.dismiss()


                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }



    private fun attachMenu(layout: FilterMenuLayout): FilterMenu? {
        return FilterMenu.Builder(requireActivity())
            .addItem(R.drawable.ic_menu_tickets)
            .addItem(R.drawable.ic_menu_categories)
            .addItem(R.drawable.ic_menu_user)
            .attach(layout)
            .withListener(listener)
            .build()
    }

    override fun resultSuccess(result: String) {

    }
}