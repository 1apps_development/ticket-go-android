package com.workdo.ticketgo.fragment

import android.app.Activity
import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.ImageListAdapter
import com.workdo.ticketgo.databinding.FragReplayTicketBinding
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.BottomSheetCallBack
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

class FragReplayTicket : BottomSheetDialogFragment() {
    private val mBehavior: BottomSheetBehavior<View>? = null
    private var callback: BottomSheetCallBack? = null
    private var _binding: FragReplayTicketBinding? = null
    private lateinit var filePickerLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var imageAdapter: ImageListAdapter
    private val imageList = ArrayList<Uri>()

    private val binding get() = _binding!!
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)

        mBehavior = BottomSheetBehavior.from(view?.parent as View)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragReplayTicketBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getString("id")
        Log.e("id",id.toString())

        binding.constraintChooseFav.setOnClickListener {
            selectFiles()
        }

        binding.btnReplay.setOnClickListener {
            if (binding.edDescription.text.toString().isEmpty()) {
                Utils.errorAlert(requireActivity(), "Description Is Required")

            } else if (imageList.size == 0) {
                Utils.errorAlert(requireActivity(), "Image Attachment Is Required")

            }else
            {
                replayTicket(id.toString())
            }

        }

        binding.btnClose.setOnClickListener {
            dialog?.dismiss()
        }


        imageAdapter = ImageListAdapter(imageList)
        setupImageListAdapter()
        filePickerLauncher =
            registerForActivityResult(ActivityResultContracts.OpenMultipleDocuments()) { uris ->
                uris?.let {
                    for (uri in uris) {
                        imageList.add(uri)
                    }
                    imageAdapter.notifyDataSetChanged()

                }

            }

    }

    private fun replayTicket(id: String) {


        val ticketId = Utils.setRequestBody(id)
        val userId = Utils.setRequestBody(SharePreference.getStringPref(requireActivity(),SharePreference.userId)?:"")
        val replyDescription = Utils.setRequestBody(binding.edDescription.text.toString())


        val imageParts = ArrayList<MultipartBody.Part>()
        for (uri in 0 until imageList.size) {

            val file = File(imageList[uri].path.toString())

            val requestFile = file.asRequestBody("image/*".toMediaType())
            val part = MultipartBody.Part.createFormData("reply_attachments[]", file.name, requestFile)
            imageParts.add(part)
        }



        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity()).replayTicket(userId,ticketId,replyDescription,imageParts)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            dialog?.dismiss()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun setupImageListAdapter() {
        binding.rvImageList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = imageAdapter
        }
    }

    private val pickMedia =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
            if (activityResult.resultCode == Activity.RESULT_OK) {
                activityResult.data?.data?.let { imageList.add(it) }
                imageAdapter.notifyDataSetChanged()
            }
        }


    private fun selectFiles() {
        ImagePicker.with(requireActivity()).cropSquare().compress(1024)
            .saveDir(File(requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                resources.getString(R.string.app_name))).maxResultSize(512, 512).createIntent {
                pickMedia.launch(it)
            }
    }

    override fun onStart() {
        super.onStart()
        mBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Utils.dismissLoadingProgress()

    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}