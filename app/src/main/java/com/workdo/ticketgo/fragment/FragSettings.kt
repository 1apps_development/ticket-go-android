package com.workdo.ticketgo.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.workdo.ticketgo.R
import com.workdo.ticketgo.activity.*
import com.workdo.ticketgo.databinding.FragSettingsBinding
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.SharePreference.Companion.setStringPref
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch


class FragSettings : Fragment() {
    private  var _binding: FragSettingsBinding?=null

    private val binding get()=_binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragSettingsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListener()

    }


    private fun callLogOutApi(loginRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).logOut(loginRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            val preference = SharePreference(requireActivity())

                            preference.mLogout()

                            setStringPref(requireActivity(), SharePreference.userId, "")

                            val intent = Intent(activity, ActIntro::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            requireActivity().startActivity(intent)
                            requireActivity().finish()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }


                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun initClickListener() {
        binding.constraintEmailSettings.setOnClickListener {
            startActivity(Intent(requireActivity(), ActEmailSettings::class.java))
        }

        binding.constraintSiteSettings.setOnClickListener {
            startActivity(Intent(requireActivity(), ActSiteSettings::class.java))
        }
        binding.constraintTicketField.setOnClickListener {
            startActivity(Intent(requireActivity(), ActTicketFields::class.java))
        }

        binding.constraintRecaptcha.setOnClickListener {
            startActivity(Intent(requireActivity(), ActReCaptcha::class.java))

        }

        binding.clLogOut.setOnClickListener {
            val request = HashMap<String, String>()
            request["id"] = SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""
            callLogOutApi(request)
        }
    }


}