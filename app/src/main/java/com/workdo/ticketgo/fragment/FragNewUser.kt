package com.workdo.ticketgo.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.FragNewUserBinding
import com.workdo.ticketgo.model.UsersDetail
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.BottomSheetCallBack
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch
import java.io.File


class FragNewUser : BottomSheetDialogFragment() {
    private lateinit var binding: FragNewUserBinding
    private var callback: BottomSheetCallBack? = null
    private var imageFile: File? = null
    private var role=""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragNewUserBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()

        val from = arguments?.getString("type")
        if (from == "UPDATE") {
            binding.tvAdminTitle.text=resources.getString(R.string.edit_user_)
            binding.tvInvite.text=resources.getString(R.string.update)
            binding.tvTitle.text=resources.getString(R.string.edit_)
            getUserDetail()
        }

    }
    fun setCallback(callback: BottomSheetCallBack) {
        this.callback = callback
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
    }

    private fun initClickListeners() {
        binding.ivClose.setOnClickListener {
            dismiss()
        }

        binding.clUploadImage.setOnClickListener {
            ImagePicker.with(requireActivity())
                .cropSquare()
                .compress(1024)
                .saveDir(File(requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    resources.getString(R.string.app_name)))
                .maxResultSize(1080, 1080)

                .createIntent { intent ->

                    startActivityForResult(intent, 200)
                }
        }
        Log.e("id",arguments?.getString("user_id")?:"")

        binding.constraintInvite.setOnClickListener {
            if(binding.edFullName.text.isNullOrEmpty())
            {
                Utils.errorAlert(requireActivity(),"Name Shouldn't be empty")
            }else if(binding.edEmail.text.isNullOrEmpty())
            {
                Utils.errorAlert(requireActivity(),"Email Shouldn't be empty")
            }else if(binding.edPassword.text.isNullOrEmpty())
            {
                Utils.errorAlert(requireActivity(),"Password Shouldn't be empty")

            }else if(binding.edConfirmPassword.text.isNullOrEmpty())
            {
                Utils.errorAlert(requireActivity(),"Confirm Password Shouldn't be empty")

            }else if(binding.edPassword.text.toString() != binding.edConfirmPassword.text.toString())
            {
                Utils.errorAlert(requireActivity(),"Confirm Password Must be Same As Password")

            }else
            {
                val from = arguments?.getString("type")
                if (from == "UPDATE") {
                    createUser(arguments?.getString("user_id")?:"")

                }else
                {
                    createUser("")

                }

            }
        }

    }

    private fun getUserDetail() {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity()).getUserDetail(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            binding.edEmail.setText(response.body.data?.users?.email.toString())
                            binding.edFullName.setText(response.body.data?.users?.name.toString())



//                            binding.edFullName.setText(response.body.data?.users?.name.toString())




                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(),
                                response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    fun setupData(userDAta: UsersDetail) {
        binding.edEmail.setText(userDAta.email.toString())
        binding.edFullName.setText(userDAta.name.toString())

    }



    private fun createUser(id:String) {
        val userId =Utils.setRequestBody(id)
        val name =Utils.setRequestBody(binding.edFullName.text.toString())
        val email =Utils.setRequestBody(binding.edEmail.text.toString())
        val password =Utils.setRequestBody(binding.edPassword.text.toString())
        val confirmPassword =Utils.setRequestBody(binding.edConfirmPassword.text.toString())
        val userRole=Utils.setRequestBody(role)

        val file = imageFile?.let {
            Utils.setImageUpload("avatar", it)
        }
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).createUser(userId,name,userRole,email,password,confirmPassword,file)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            callback?.resultSuccess("UpdateData")
                            dialog?.dismiss()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(),
                                response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val fileUri = data?.data!!

            when (requestCode) {
                200 -> {
                    fileUri.path.let {
                        imageFile = File(it.toString())
                    }
                    Log.e("filepath",imageFile?.path.toString())
                    binding.tvImageFilename.text =
                        Utils.getFileNameFromUri(requireActivity().contentResolver, data.data!!).toString()
                }
            }
        }
    }
}



