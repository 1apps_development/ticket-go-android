package com.workdo.ticketgo.fragment

import android.app.AlertDialog.THEME_DEVICE_DEFAULT_DARK
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.skydoves.colorpickerview.ColorEnvelope
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener
import com.skydoves.colorpickerview.listeners.ColorListener

import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.CategoriesAdapter
import com.workdo.ticketgo.adapter.ChartCategoryListAdapter
import com.workdo.ticketgo.databinding.FragCategoriesBinding
import com.workdo.ticketgo.databinding.NewCategoryDialogBinding
import com.workdo.ticketgo.model.CategoryAnalyticsItem
import com.workdo.ticketgo.model.CategoryDataItem
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.ColorPickerDialog
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch
import okhttp3.internal.cache2.Relay.Companion.edit


class FragCategories : Fragment() {
    private var _binding: FragCategoriesBinding? = null
    private val binding get() = _binding!!
    private val colorPickerDialog: ColorPickerDialog by lazy {
        ColorPickerDialog(requireActivity())
    }

    private var getCategoriesList = ArrayList<CategoryDataItem>()
    private lateinit var categoryAdapter: CategoriesAdapter
    private lateinit var chartCategoriesAdapter: ChartCategoryListAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragCategoriesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        categoryAdapter = CategoriesAdapter(getCategoriesList) { pos, type ->
            if (type == Constants.DELETE) {
                deleteCategory(pos, getCategoriesList[pos].id.toString())
            } else if (type == Constants.UPDATE) {
                newCategoryDialog(Constants.UPDATE, getCategoriesList[pos])
            }

        }
        setupAdapter()


        getCategory()


        binding.pieChart.description.isEnabled = false
        binding.pieChart.setUsePercentValues(false)
        binding.pieChart.setEntryLabelTextSize(12f)
        binding.pieChart.centerText = ""
        binding.pieChart.isDrawHoleEnabled = true
        binding.pieChart.setCenterTextSize(22f)
        binding.pieChart.legend.isEnabled = false


    }


    private fun getCategory() {
        val request = HashMap<String, String>()
        request["id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).getCategory()) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.category?.data?.let { getCategoriesList.addAll(it) }


                            if ((response.body.data?.categoryAnalytics?.size ?: 0) > 0) {
                                response.body.data?.categoryAnalytics?.let { setData(it) }
                            }else
                            {
                                val categoriesList= ArrayList<CategoryAnalyticsItem>()
                                categoriesList.add(CategoryAnalyticsItem("#FC275A","Bug",190))
                                categoriesList.add(CategoryAnalyticsItem("#153364","Resolved",120))
                                categoriesList.add(CategoryAnalyticsItem("#6FD943","Errors",90))
                                setData(categoriesList)
                            }


                            categoryAdapter.notifyDataSetChanged()

                            response.body.data?.categoryAnalytics?.let {
                                setupChartCategoryAdapter(it)
                            }

                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun deleteCategory(pos: Int, categoryId: String) {
        val request = HashMap<String, String>()
        request["id"] = categoryId

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).deleteCategory(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            getCategoriesList.removeAt(pos)
                            categoryAdapter.notifyDataSetChanged()
                            chartCategoriesAdapter.notifyDataSetChanged()


                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun addCategory(
        name: String,
        colorCode: String,
        id: String,
        dialog: BottomSheetDialog,
    ) {
        val request = HashMap<String, String>()
        request["name"] = name
        request["id"] = id
        request["user_id"] = SharePreference.getStringPref(requireActivity(),SharePreference.userId)?:""
        request["color"] = colorCode

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).createCategory(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            dialog.dismiss()
                            val data = response.body.data
                            getCategoriesList.add(CategoryDataItem(color = data?.category?.color.toString(),
                                id = data?.category?.id,
                                name = data?.category?.name.toString()))
                            categoryAdapter.notifyDataSetChanged()
                            chartCategoriesAdapter.notifyDataSetChanged()

                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if(response.code==401)
                    {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    }else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun setupAdapter() {
        binding.rvCategories.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = categoryAdapter
        }
    }


    private fun setupChartCategoryAdapter(categoryAnalytics: ArrayList<CategoryAnalyticsItem>) {

        chartCategoriesAdapter=ChartCategoryListAdapter(categoryAnalytics)
        binding.rvLegend.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = chartCategoriesAdapter
        }
    }

    private fun initClickListeners() {
        binding.btnAddNewCategory.setOnClickListener {
            newCategoryDialog(Constants.CREATE, CategoryDataItem())
        }
    }


    private fun newCategoryDialog(type: String, data: CategoryDataItem) {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        val bottomSheetBinding = NewCategoryDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        if (type == Constants.UPDATE) {
            bottomSheetBinding.tvCreate.text = resources.getString(R.string.edit_)
            bottomSheetBinding.tvNewCategoryTitle.text = resources.getString(R.string.edit_category)
            bottomSheetBinding.edCategoryName.setText(data.name.toString())
            bottomSheetBinding.tvColorCode.setBackgroundColor(Color.parseColor(data.color))
            bottomSheetBinding.tvColorCode.text = data.color
            bottomSheetBinding.btnCreate.text =resources.getString(R.string.update_)
        } else {
            val color = ResourcesCompat.getColor(resources, R.color.lime_green, null)
            val colorHex = String.format("#%06X", color)

            bottomSheetBinding.tvColorCode.text = colorHex
        }


        bottomSheetBinding.tvColorCode.setOnClickListener {
            showColorPickerDialog(bottomSheetBinding.tvColorCode)
        }
        bottomSheetBinding.btnCreate.setOnClickListener {
            if (bottomSheetBinding.edCategoryName.text?.isEmpty() == true) {
                Utils.errorAlert(requireActivity(), "Category Name is Required")
            } else {

                if (type == Constants.CREATE) {
                    addCategory(bottomSheetBinding.edCategoryName.text.toString(),
                        bottomSheetBinding.tvColorCode.text.toString(),
                        "",
                        bottomSheetDialog)

                } else {
                    addCategory(bottomSheetBinding.edCategoryName.text.toString(),
                        bottomSheetBinding.tvColorCode.text.toString(),
                        data.id.toString(),
                        bottomSheetDialog)

                }

            }
        }
        bottomSheetDialog.show()
    }


    private fun setData(categoryAnalytics: ArrayList<CategoryAnalyticsItem>) {
        val entries: MutableList<PieEntry> = ArrayList()
        val colors = ArrayList<Int>()
        for (i in 0 until categoryAnalytics.size) {
            entries.add(PieEntry(categoryAnalytics[i].value?.toFloat() ?: 0.0f,
                categoryAnalytics[i].category))
            colors.add(Color.parseColor(categoryAnalytics[i].color))
        }

        val dataSet = PieDataSet(entries, "Categories")

        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f
        dataSet.colors = colors.toMutableList()
        val data = PieData(dataSet)

        binding.pieChart.data = data
        binding.pieChart.data.dataSet.setDrawValues(false)
        binding.pieChart.invalidate()
    }

    private fun showColorPickerDialog(textview: AppCompatTextView) {
        colorPickerDialog.showColorPickerDialog { color ->
            val hexColor = String.format("#%06X", 0xFFFFFF and color)
            textview.setBackgroundColor(color)
            textview.text = hexColor

        }
    }


}