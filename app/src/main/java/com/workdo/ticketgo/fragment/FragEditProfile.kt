package com.workdo.ticketgo.fragment

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.ticketgo.R
import com.workdo.ticketgo.databinding.FragEditProfileBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.BottomSheetCallBack
import com.workdo.ticketgo.util.Constants
import com.workdo.ticketgo.util.ExtensionFunctions.loadFile
import com.workdo.ticketgo.util.ExtensionFunctions.loadUrlCircle
import com.workdo.ticketgo.util.SharePreference
import com.workdo.ticketgo.util.Utils
import kotlinx.coroutines.launch
import java.io.File


class FragEditProfile : BottomSheetDialogFragment() {
    private var _binding: FragEditProfileBinding? = null
    private var callback: BottomSheetCallBack? = null
    private var imageFile: File? = null
    private var role = ""

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragEditProfileBinding.inflate(layoutInflater)
        return binding.root
    }

    fun setCallback(callback: BottomSheetCallBack) {
        this.callback = callback
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.edFullName.setText(SharePreference.getStringPref(requireActivity(),
            SharePreference.userName))
        binding.edEmail.setText(SharePreference.getStringPref(requireActivity(),
            SharePreference.userEmail))

        binding.ivProfile.loadUrlCircle(SharePreference.getStringPref(requireActivity(),
            SharePreference.userProfile))
        binding.btnUpload.setOnClickListener {
            ImagePicker.with(requireActivity())
                .cropSquare()
                .compress(1024)
                .saveDir(File(requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    resources.getString(R.string.app_name)))
                .maxResultSize(1080, 1080)

                .createIntent { intent ->
                    pickMedia.launch(intent)
                  }
        }

        binding.ivClose.setOnClickListener {
            dialog?.dismiss()
        }

        binding.btnUpdate.setOnClickListener {
            if (Constants.IsDemoMode) {

                Utils.errorAlert(requireActivity(),resources.getString(R.string.demo_user_message))
            } else {
                if (binding.edFullName.text.toString().isEmpty()) {
                    Utils.errorAlert(requireActivity(), "Name Shouldn't be empty")

                } else if (binding.edEmail.text.toString().isEmpty()) {
                    Utils.errorAlert(requireActivity(), "Email Shouldn't be empty")

                } else if (binding.edPassword.text.isNullOrEmpty()) {
                    Utils.errorAlert(requireActivity(), "Password Shouldn't be empty")

                } else if (binding.edConfirmPassword.text.isNullOrEmpty()) {
                    Utils.errorAlert(requireActivity(), "Confirm Password Shouldn't be empty")

                } else if (binding.edPassword.text.toString() != binding.edConfirmPassword.text.toString()) {
                    Utils.errorAlert(requireActivity(), "Confirm Password Must be Same As Password")

                } else {
                    updateUser()
                }
            }

        }

    }

    private fun updateUser() {
        val userId = Utils.setRequestBody(SharePreference.getStringPref(requireActivity(),
            SharePreference.userId) ?: "")
        val name = Utils.setRequestBody(binding.edFullName.text.toString())
        val email = Utils.setRequestBody(binding.edEmail.text.toString())
        val password = Utils.setRequestBody(binding.edPassword.text.toString())
        val confirmPassword = Utils.setRequestBody(binding.edConfirmPassword.text.toString())
        val userRole = Utils.setRequestBody("2")

        val file = imageFile?.let {
            Utils.setImageUpload("avatar", it)
        }
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .createUser(userId, name, userRole, email, password, confirmPassword, file)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(requireActivity(),SharePreference.userProfile,response.body.data?.user?.avatar.toString())
                            SharePreference.setStringPref(requireActivity(),SharePreference.userName,response.body.data?.user?.name.toString())
                            SharePreference.setStringPref(requireActivity(),SharePreference.userEmail,response.body.data?.user?.email.toString())

                            callback?.resultSuccess("UpdateData")
                            dialog?.dismiss()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(),
                                response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())

                    } else {
                    Utils.errorAlert(requireActivity(), response.body.message.toString())
                }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private val pickMedia =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->

            if (activityResult.resultCode == Activity.RESULT_OK) {
                Log.e("uri",activityResult.data?.data.toString())
                imageFile = activityResult.data?.data?.path?.let { File(it) }
                binding.ivProfile.loadFile(imageFile)

            }
        }

}