package com.workdo.ticketgo.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.ticketgo.R
import com.workdo.ticketgo.adapter.UserAdapter
import com.workdo.ticketgo.databinding.FragUsersBinding
import com.workdo.ticketgo.model.UsersDataItem
import com.workdo.ticketgo.networking.ApiClient
import com.workdo.ticketgo.remote.NetworkResponse
import com.workdo.ticketgo.util.*
import com.workdo.ticketgo.util.ExtensionFunctions.hide
import com.workdo.ticketgo.util.ExtensionFunctions.show
import kotlinx.coroutines.launch

class FragUsers : Fragment(), BottomSheetCallBack {
    private  var _binding: FragUsersBinding?=null
    private var userAdapter: UserAdapter? = null
    private var userList = ArrayList<UsersDataItem>()
    private var linearLayoutManager: LinearLayoutManager? = null
    private var isLoading = false
    private var isLastPage = false
    private var currentPage = 1
    private var totalPages: Int = 0


    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragUsersBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        linearLayoutManager = LinearLayoutManager(requireActivity())
        userAdapter = UserAdapter(userList) { pos, type ->
            if (type == Constants.DELETE) {
                callDeleteUser(pos)
            }else if(type==Constants.UPDATE)
            {
                val fragNewUser = FragNewUser()
                val bundle=Bundle()
                bundle.putString("type","UPDATE")
                bundle.putString("user_id",userList[pos].id.toString())
                fragNewUser.arguments=bundle


                fragNewUser.setCallback(this)
                fragNewUser.show(requireActivity().supportFragmentManager, FragNewUser::class.java.name)
            }
        }
        setupAdapter()
        initClickListeners()
        callGetUsers()
    }


    private fun callGetUsers() {
        val request = HashMap<String, String>()
        request["user_id"] = SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""
        request["search"] = ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity()).getUsers(currentPage.toString(), request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            currentPage = response.body.data?.users?.currentPage!!.toInt()
                            totalPages = response.body.data.users.lastPage!!.toInt()
                            response.body.data.users.data?.let { userList.addAll(it) }
                            if (currentPage >= totalPages) {
                                isLastPage = true
                            }
                            isLoading = false

                            userAdapter?.notifyDataSetChanged()
                            if(userList.size>0)
                            {
                                binding.rvUsers.show()
                                binding.tvNoDataFound.hide()
                                binding.rvUsers.smoothScrollToPosition(userList.size-1)

                            }else
                            {
                                binding.rvUsers.hide()
                                binding.tvNoDataFound.show()
                            }

                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    private fun callDeleteUser(pos: Int) {
        val request = HashMap<String, String>()
        request["id"] = SharePreference.getStringPref(requireActivity(), SharePreference.userId) ?: ""

        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).deleteUser(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            userList.removeAt(pos)
                            userAdapter?.notifyDataSetChanged()
                            if(userList.size==0)
                            {
                                binding.rvUsers.hide()
                                binding.tvNoDataFound.show()
                            }

                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.code == 401) {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                    }

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }
    private fun setupAdapter() {

        binding.rvUsers.apply {
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    private fun initClickListeners() {
        binding.btnAddNewUser.setOnClickListener {
            val fragNewUser = FragNewUser()
            fragNewUser.setCallback(this)
            fragNewUser.show(requireActivity().supportFragmentManager, FragNewUser::class.java.name)

        }


        binding.btnShowMoreUser.setOnClickListener {
            isLoading = true
            currentPage++
            callGetUsers()
        }
    }

    override fun resultSuccess(result: String) {
        currentPage = 1
        userList.clear()
        callGetUsers()
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}