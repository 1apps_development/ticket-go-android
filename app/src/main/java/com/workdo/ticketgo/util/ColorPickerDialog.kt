package com.workdo.ticketgo.util

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.skydoves.colorpickerview.ColorPickerDialog
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener

class ColorPickerDialog(private val context: Context) {

    fun showColorPickerDialog(onColorSelected: (color: Int) -> Unit) {
        val builder = ColorPickerDialog.Builder(context)
            .setTitle("Pick a color")
            .setPositiveButton("OK", ColorEnvelopeListener { envelope, _ ->
                onColorSelected(envelope.color)
            })
            .setNegativeButton("Cancel") { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .attachAlphaSlideBar(true)
            .attachBrightnessSlideBar(true)
            .create()

        builder.show()
    }
}
