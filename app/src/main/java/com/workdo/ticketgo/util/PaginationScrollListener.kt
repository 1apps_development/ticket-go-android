package com.workdo.ticketgo.util

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationScrollListener(var layoutManager: LinearLayoutManager?) : RecyclerView.OnScrollListener() {

    abstract fun isLastPage(): Boolean

    abstract fun isLoading(): Boolean



    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        recyclerView.let { super.onScrolled(it, dx, dy) }

        val visibleItemCount = layoutManager?.childCount
        val totalItemCount = layoutManager?.itemCount?:0
        val firstVisibleItemPosition = layoutManager?.findFirstVisibleItemPosition()?:0

        Log.e("firstVisibleItemCount",firstVisibleItemPosition.toString())
        Log.e("visibleItemCount",visibleItemCount.toString())
        if (!isLoading() && !isLastPage()) {
            if (visibleItemCount != null ) {
                if (visibleItemCount >= totalItemCount && firstVisibleItemPosition >= 0) {
                    loadMoreItems()
                }
            }
        }
    }
    abstract fun loadMoreItems()
}