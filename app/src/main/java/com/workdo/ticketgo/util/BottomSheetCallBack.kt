package com.workdo.ticketgo.util

interface BottomSheetCallBack {

    fun resultSuccess(result:String)

}

interface DeleteTicketCallBack
{
    fun deleteCallBack(pos:Int)
}