package com.workdo.ticketgo.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.UnderlineSpan
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.net.ParseException

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.google.android.material.snackbar.Snackbar
import com.workdo.ticketgo.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object ExtensionFunctions {

    fun View.show() {
        this.visibility = View.VISIBLE
    }

    fun View.hide() {
        this.visibility = View.GONE
    }

    fun View.invisible() {
        this.visibility = View.INVISIBLE
    }

    fun String.isValidEmail(): Boolean = this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    fun Context.getDrawableImage(@DrawableRes id: Int): Drawable? {
        return ResourcesCompat.getDrawable(this.resources, id, null)
    }


    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.requestFocus()
        imm.showSoftInput(this, 0)
    }

    fun Context.startActivity(destinationClass: Class<*>) {
        startActivity(Intent(this, destinationClass))
    }
    fun Activity.startActivityClearTop(destinationClass: Class<*>) {



        val intent = Intent(this, destinationClass)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        this.finish()

    }


    fun Context.getColorName(@ColorRes id: Int): ColorStateList {
        return ColorStateList.valueOf(ResourcesCompat.getColor(this.resources, id, null))
    }

    fun Activity.hideKeyboard() {
        val imm: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = currentFocus ?: View(this)
        imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun View.isDisable() {
        this.isClickable = false
        this.isEnabled = false
    }

    fun View.isEnable() {
        this.isClickable = true
        this.isEnabled = true
    }

    fun TextView.underLine(text: String) {


        val mSpannableString = SpannableString(text)
        mSpannableString.setSpan(UnderlineSpan(), 0, mSpannableString.length, 0)

        this.text = mSpannableString
    }
    fun ImageView.loadUrl(url: String?) {
        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun ImageView.loadUrlCircle(url: String?) {
        Glide.with(context)
            .load(url)
            .circleCrop()
            .placeholder(R.drawable.ic_user_profile)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun ImageView.loadFile(url: File?) {
        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun <T> ImageView.loadCircularImage(
        model: T,
        borderSize: Float = 0F,
        borderColor: Int = Color.TRANSPARENT,
    ) {
        Glide.with(context)
            .asBitmap()
            .load(model)
            .apply(RequestOptions.circleCropTransform())
            .into(object : BitmapImageViewTarget(this) {
                override fun setResource(resource: Bitmap?) {
                    setImageDrawable(
                        resource?.run {
                            RoundedBitmapDrawableFactory.create(
                                resources,
                                if (borderSize > 0) {
                                    createBitmapWithBorder(borderSize, borderColor)
                                } else {
                                    this
                                }
                            ).apply {
                                isCircular = true
                            }
                        }
                    )
                }
            })
    }


    private fun Bitmap.createBitmapWithBorder(borderSize: Float, borderColor: Int): Bitmap {
        val borderOffset = (borderSize * 2).toInt()
        val halfWidth = width / 2
        val halfHeight = height / 2
        val circleRadius = Math.min(halfWidth, halfHeight).toFloat()
        val newBitmap = Bitmap.createBitmap(
            width + borderOffset,
            height + borderOffset,
            Bitmap.Config.ARGB_8888
        )

        val centerX = halfWidth + borderSize
        val centerY = halfHeight + borderSize

        val paint = Paint()
        val canvas = Canvas(newBitmap).apply {
            // Set transparent initial area
            drawARGB(0, 0, 0, 0)
        }

        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        canvas.drawCircle(centerX, centerY, circleRadius, paint)

        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(this, borderSize, borderSize, paint)

        paint.xfermode = null
        paint.style = Paint.Style.STROKE
        paint.color = borderColor
        paint.strokeWidth = borderSize
        canvas.drawCircle(centerX, centerY, circleRadius, paint)
        return newBitmap
    }

    fun fromHtml(source: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(source)
        }
    }


    private const val CLICK_DELAY = 500L

    class SafeViewClickListener(
        private val clickListener: ((view: View) -> Unit)?,
    ) : View.OnClickListener {
        private var isEnabled = true
        override fun onClick(v: View) {
            if (isEnabled) {
                isEnabled = false
                clickListener?.invoke(v)
                v.postDelayed(
                    { isEnabled = true },
                    CLICK_DELAY
                )
            }
        }
    }

    fun View.setSafeOnClickListener(clickListener: ((view: View) -> Unit)?) {
        setOnClickListener(SafeViewClickListener(clickListener))
    }

    fun showSnackBar(
        activity: Activity,
        message: String,
        action: String? = null,
        actionListener: View.OnClickListener? = null,
        duration: Int = Snackbar.LENGTH_SHORT
    ) {
        val snackBar = Snackbar.make(activity.findViewById(android.R.id.content), message, duration)
            .setTextColor(Color.WHITE)
        if (action != null && actionListener != null) {
            snackBar.setAction(action, actionListener)
        }
        snackBar.show()
    }

    fun Context.copyTextToClipboard(text: CharSequence, label: String = "Copied Text") {
        val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText(label, text)
        clipboardManager.setPrimaryClip(clipData)
    }


    @SuppressLint("SimpleDateFormat")
    fun String.toDate(withFormat: String = "yyyy/MM/dd hh:mm"): Date {
        val dateFormat = SimpleDateFormat(withFormat)
        var convertedDate = Date()
        try {
            convertedDate = dateFormat.parse(this)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return convertedDate
    }


}