package com.workdo.ticketgo.util

object Constants
{
    var ItemClick="ItemClick"
    var DELETE="DELETE"
    var REPLY="REPLY"
    var UPDATE="UPDATE"
    var CREATE="CREATE"
    var IsDemoMode=false
    var BASE_URL="https://client.workdo.io/mobile-app/ticketgo-saas"
}