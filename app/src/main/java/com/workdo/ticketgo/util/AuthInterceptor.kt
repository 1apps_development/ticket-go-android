package com.workdo.ticketgo.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.workdo.ticketgo.activity.ActEmailLogin
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val context: Context) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        if (response.code == 401) {
            Utils.setInvalidToken(context as Activity)

        }

        return response
    }
}