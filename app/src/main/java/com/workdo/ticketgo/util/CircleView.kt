package com.workdo.ticketgo.util

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class CircleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var circleColor = Color.RED

    init {
        // Set initial background color
        setBackgroundColor(circleColor)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val centerX = width / 2f
        val centerY = height / 2f
        val radius = centerX.coerceAtMost(centerY)

        // Draw the circle
        paint.color = circleColor
        canvas.drawCircle(centerX, centerY, radius, paint)
    }

    fun setColor(selectedColor: Int) {
        circleColor = selectedColor
        setBackgroundColor(circleColor)
        invalidate()
    }
}