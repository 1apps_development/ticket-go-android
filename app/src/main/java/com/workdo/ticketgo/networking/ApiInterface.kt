package com.workdo.ticketgo.networking

import com.workdo.ticketgo.model.*
import com.workdo.ticketgo.remote.NetworkResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface {

    @POST("login")
    suspend fun getLogin(@Body map: HashMap<String, String>): NetworkResponse<LoginResponse, SingleResponse>

    @POST("home")
    suspend fun home(@Body map:HashMap<String,String>):NetworkResponse<HomePageResponse,SingleResponse>

    @GET("lang")
    suspend fun languages():NetworkResponse<LanguageResponse,SingleResponse>

    @POST("logout")
    suspend fun logOut(@Body map:HashMap<String,String>):NetworkResponse<SingleResponse,SingleResponse>

    @POST("faq")
    suspend fun faqListApi(@Query("page") page: String,@Body map:HashMap<String,String>):NetworkResponse<FaqResponse,SingleResponse>

    @POST("faq_create")
    suspend fun createFaq(@Body map:HashMap<String,String>):NetworkResponse<FaqResponse,SingleResponse>

    @POST("site_setting_page")
    suspend fun siteSettings(@Body map :HashMap<String,String>):NetworkResponse<SiteSettingsResponse,SingleResponse>


    @POST("ticketsetting")
    suspend fun ticketSettings(@Body map:HashMap<String,String>):NetworkResponse<TicketSettingsResponse,SingleResponse>

    @POST("users")
    suspend fun getUsers(@Query("page") page: String,@Body map: HashMap<String, String>):NetworkResponse<UsersPageResponse,SingleResponse>

    @POST("getuser")
    suspend fun getUserDetail(@Body map: HashMap<String, String>):NetworkResponse<GetUserDetailResponse,SingleResponse>


    @POST("user_delete")
    suspend fun deleteUser(@Body map :HashMap<String,String>):NetworkResponse<SingleResponse,SingleResponse>

    @POST("faq_delete")
    suspend fun deleteFaq(@Body map :HashMap<String,String>):NetworkResponse<SingleResponse,SingleResponse>



/*    @POST("user_create")
    suspend fun createUser(@Body map :HashMap<String,String>):NetworkResponse<SingleResponse,SingleResponse>*/

    @POST("user_create")
    @Multipart
    suspend fun createUser(
        @Part("id") id: RequestBody,
        @Part("name") name: RequestBody,
        @Part("role") role: RequestBody,
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody,
        @Part("password_confirmation") password_confirmation: RequestBody,
        @Part avatar: MultipartBody.Part?
    ): NetworkResponse<CreateUserResponse, SingleResponse>



    @GET("role")
    suspend fun getRole():NetworkResponse<GetRoleResponse,SingleResponse>

    @POST("recaptchasetting")
    suspend fun reCaptchaSettings(@Body map:HashMap<String,String>):NetworkResponse<RecaptchaSettingsResponse,SingleResponse>


    @POST("test_email_send")
    suspend fun sendTestEmail(@Body map :HashMap<String,String>) :NetworkResponse<SingleResponse,SingleResponse>

    @POST("emailsetting")
    suspend fun emailSetting(@Body map :HashMap<String,String>) :NetworkResponse<SingleResponse,SingleResponse>


    @POST("emailsettingpage")
    suspend fun emailSettingPage(@Body map :HashMap<String,String>) :NetworkResponse<EmailSettingsPageResponse,SingleResponse>

    @GET("category")
    suspend fun getCategory():NetworkResponse<CategoryResponse,SingleResponse>

    @POST("getcategory")
    suspend fun ticketCategory(@Query("page") page: String,@Body map: HashMap<String, String>):NetworkResponse<GetCategoryResponse,SingleResponse>

    @POST("create_category")
    suspend fun createCategory(@Body map :HashMap<String,String>):NetworkResponse<CreateCategoryResponse,SingleResponse>


    @POST("delete_category")
    suspend fun deleteCategory(@Body map :HashMap<String,String>):NetworkResponse<SingleResponse,SingleResponse>

    @POST("coustomfield")
    suspend fun addCustomField(@Body map :HashMap<String,String>):NetworkResponse<AddFieldResponse,SingleResponse>

    @POST("getcoustomfield")
    suspend fun getCustomField(@Body map :HashMap<String,String>):NetworkResponse<GetCustomFieldResponse,SingleResponse>

    @POST("deletecoustomfield")
    suspend fun deleteCustomField(@Body map: HashMap<String, String>):NetworkResponse<SingleResponse,SingleResponse>

    @Multipart
    @POST("sitesetting")
    suspend fun siteSettingPage(
        @Part("id") id: RequestBody,
        @Part("app_name") appName: RequestBody,
        @Part("default_language") name: RequestBody,
        @Part("site_rtl") siteRtl: RequestBody,
        @Part("gdpr_cookie") gdprCookie: RequestBody,
        @Part("cookie_text") cookieText: RequestBody,
        @Part("footer_text") footerText: RequestBody,
        @Part favicon: MultipartBody.Part?,
        @Part logo: MultipartBody.Part?,
        @Part white_logo: MultipartBody.Part?,
    ): NetworkResponse<SingleResponse, SingleResponse>
    @Multipart
    @POST("ticket_create")
    suspend fun ticketCreate(
        @Part("id") id: RequestBody,
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("category") category: RequestBody,
        @Part("subject") subject: RequestBody,
        @Part("status") status: RequestBody,
        @Part("description") description: RequestBody,
        @Part attachments: ArrayList<MultipartBody.Part>,
        @PartMap otherData: Map<String, @JvmSuppressWildcards RequestBody>

    ): NetworkResponse<SingleResponse, SingleResponse>

    @POST("ticket")
    suspend fun ticket(@Body map:HashMap<String,String>):NetworkResponse<TicketPageResponse,SingleResponse>


    @POST("ticket_delete")
    suspend fun deleteTicket(@Body map:HashMap<String,String>):NetworkResponse<DeleteTicketResponse,SingleResponse>


    @Multipart
    @POST("replayticket")
    suspend fun replayTicket(
        @Part("id") id: RequestBody,
        @Part("ticket_id") name: RequestBody,
        @Part("reply_description") email: RequestBody,
        @Part attachments: ArrayList<MultipartBody.Part>
    ): NetworkResponse<ReplyTicketResponse, SingleResponse>


}