package com.workdo.ticketgo.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.FragmentActivity
import com.workdo.ticketgo.util.Utils


abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        setContentView(setLayout())
    }

    protected abstract fun setLayout(): View
    protected abstract fun initView()

    open fun openActivity(destinationClass: Class<*>) {
        startActivity(Intent(this@BaseActivity, destinationClass))
    }

    override fun onDestroy() {
        super.onDestroy()
        Utils.dismissLoadingProgress()
    }
}





